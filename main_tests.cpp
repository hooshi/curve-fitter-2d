#include <cstdio>

namespace deadlinecodetest
{
namespace bezier
{
extern int
test_vtk_writer(int argc, char ** argv);
extern int
test_bezier(int argc, char ** argv);
} // namespace bezier

namespace globfitter
{
extern int
test_curves(int argc, char ** argv);
extern int
test_objectives(int argc, char ** argv);
extern int
example(int argc, char ** argv);
}

int
main(int argc, char ** argv)
{
  printf(" ===================== ------ ==================== \n");
  printf(" ===================== BEZIER ==================== \n");
  printf(" ===================== ------ ==================== \n");

  printf("========== TESTING VTK WRITER =========== \n");
  ::deadlinecodetest::bezier::test_vtk_writer(argc, argv);
  printf("==========        DONE        =========== \n");

  printf("========== TESTING BEZIER     =========== \n");
  ::deadlinecodetest::bezier::test_bezier(argc, argv);
  printf("==========        DONE        =========== \n");

  printf(" ===================== ------ ==================== \n");
  printf(" ===================== GLOB FITTER ==================== \n");
  printf(" ===================== ------ ==================== \n");

  printf("========== TESTING CURVES =========== \n");
  ::deadlinecodetest::globfitter::test_curves(argc, argv);
  printf("==========        DONE        =========== \n");

  printf("========== TESTING OBJECTIVES =========== \n");
  ::deadlinecodetest::globfitter::test_objectives(argc, argv);
  printf("==========        DONE        =========== \n");

  printf("==========        EXAMPLE     =========== \n");
  ::deadlinecodetest::globfitter::example(argc, argv);
  printf("==========        DONE        =========== \n");

  return 0;
}
} // namespace deadlinecodetest

// used by the makefile
#ifdef DEADLINECODE_INDEPENDANT_EXE
int
main(int argc, char ** argv)
{
  return deadlinecodetest::main(argc, argv);
}
#endif // DEADLINECODE_INDEPENDANT_EXE
