#include <cassert>
#include <cstdio>
#include <cstdlib>

#include <limits>
#include <stdexcept>


#include <fitter_2d/bezier/bezier.hpp>
#include <fitter_2d/share/projection_derivative.hpp>
#include <fitter_2d/share/util.hpp>


// ===================================================================
//                            Bezier Curve
// ===================================================================

Eigen::Vector2d
BezierCurve::pos(const double t) const
{
  return _bezier_value_at(_control_points_d0, t);
}

Eigen::Vector2d
BezierCurve::dposdt(const double t) const
{
  return _bezier_value_at(_control_points_d1, t);
}

Eigen::Vector2d
BezierCurve::dposdtdt(const double t) const
{
  return _bezier_value_at(_control_points_d2, t);
}

Eigen::Matrix2Xd
BezierCurve::dposdparams(const double t) const
{
  const int order = 3;
  const int n_points = n_control_points();

  // Eigen::Matrix2Xd ans(2, n_points);

  // Coefficient of bernstein monomials in a bernstein polynomial
  const double b0[] = {1., 0., 0., 0.};
  const double b1[] = {0., 1., 0., 0.};
  const double b2[] = {0., 0., 1., 0.};
  const double b3[] = {0., 0., 0., 1.};

  // Value of derivatives
  const double dxdp0 = _bernstein_value_at(t, b0, order);
  const double dxdp1 = _bernstein_value_at(t, b1, order);
  const double dxdp2 = _bernstein_value_at(t, b2, order);
  const double dxdp3 = _bernstein_value_at(t, b3, order);

  Eigen::Matrix2Xd ans = Eigen::Matrix2Xd::Zero(2, n_points * 2);
  ans.row(0) << dxdp0, 0, dxdp1, 0, dxdp2, 0, dxdp3, 0;
  ans.row(1) << 0, dxdp0, 0, dxdp1, 0, dxdp2, 0, dxdp3;

  return ans;
}

Eigen::Matrix2Xd
BezierCurve::dposdtdparams(const double t) const
{
  const int order = 3;
  const double drder = 3;
  const int n_points = n_control_points();

  // Eigen::Matrix2Xd ans(2, n_points);

  // Coefficient of bernstein monomials in a bernstein polynomial
  const double b0_deriv[] = {-drder, 0., 0., 0.};
  const double b1_deriv[] = {drder, -drder, 0., 0.};
  const double b2_deriv[] = {0., drder, -drder, 0.};
  const double b3_deriv[] = {0., 0., drder, 0.};

  // Value of derivatives
  const double dxdtdp0 = _bernstein_value_at(t, b0_deriv, order - 1);
  const double dxdtdp1 = _bernstein_value_at(t, b1_deriv, order - 1);
  const double dxdtdp2 = _bernstein_value_at(t, b2_deriv, order - 1);
  const double dxdtdp3 = _bernstein_value_at(t, b3_deriv, order - 1);

  Eigen::Matrix2Xd ans = Eigen::Matrix2Xd::Zero(2, n_points * 2);
  ans.row(0) << dxdtdp0, 0, dxdtdp1, 0, dxdtdp2, 0, dxdtdp3, 0;
  ans.row(1) << 0, dxdtdp0, 0, dxdtdp1, 0, dxdtdp2, 0, dxdtdp3;

  return ans;
}

double
BezierCurve::project(const Eigen::Vector2d & point) const
{

  // int closest_idx = -1;
  double closest_t = -1;
  double closest_dist2 = std::numeric_limits<double>::max();

  // First find an initial guess for the closest point
  for(int i = 0; i < _tesselation.cols(); ++i)
  {
    double dist2 = (point - _tesselation.col(i).tail<2>()).squaredNorm();

    if(dist2 < closest_dist2)
    {
      // closest_idx = i;
      closest_t = _tesselation.col(i)(0);
      closest_dist2 = dist2;
    }
  }

  // Now refine your guess using newton iterations
  auto newton_iteration = [&](double guess) -> double {
    Eigen::Vector2d post, dert, der2t;
    post = this->pos(guess);
    dert = this->dposdt(guess);
    der2t = this->dposdtdt(guess);

    double dot = dert.dot(point - post);
    double dotDer = der2t.dot(point - post) - dert.squaredNorm();

    // Make sure the iteration does not make things worse
    if(dotDer >= -1e-30)
    {
      return guess;
    }

    // Make sure the iteration does not shoot us out of the range
    return std::max(0., std::min(1., guess - dot / dotDer));
  };
  closest_t = newton_iteration(closest_t);
  closest_t = newton_iteration(closest_t);
  closest_t = newton_iteration(closest_t);
  closest_t = newton_iteration(closest_t);

  return closest_t;
}

Eigen::VectorXd
BezierCurve::dtprojectdparams(const double time, const Eigen::Vector2d & point)
{
  const int n_params = n_control_points() * 2;
  const double tend = 1.;

  return ProjectionDerivative::eval(
      n_params, time, tend, point, pos(time), dposdt(time), dposdtdt(time), dposdparams(time), dposdtdparams(time));
}

Eigen::Matrix2Xd
BezierCurve::dposprojectdparams(const double t, const Eigen::VectorXd & dtprojectdparams)
{
  return dposdparams(t) + dposdt(t) * dtprojectdparams.transpose();
}

Eigen::Matrix2Xd
BezierCurve::dposdtprojectdparams(const double t, const Eigen::VectorXd & dtprojectdparams)
{
  return dposdtdparams(t) + dposdtdt(t) * dtprojectdparams.transpose();
}


void
BezierCurve::set_control_points(const Eigen::Matrix2Xd & control_points_d0_in)
{
  assert_break(control_points_d0_in.cols() == n_control_points());
  _control_points_d0 = control_points_d0_in;
  _control_points_d1 = _derivative_bezier_curve(_control_points_d0);
  _control_points_d2 = _derivative_bezier_curve(_control_points_d1);
  _tesselation = _tesselate(_control_points_d0, _n_tesselation);
}

const Eigen::Matrix2Xd &
BezierCurve::get_control_points()
{
  return _control_points_d0;
}

const Eigen::Matrix3Xd &
BezierCurve::get_tesselation3()
{
  return _tesselation;
}

const Eigen::Ref<const Eigen::Matrix2Xd>
BezierCurve::get_tesselation2()
{
  return _tesselation.bottomRows<2>();
}


//
// Taken from https://github.com/inkscape/lib2geom: src/2geom/bezier.h
// Compute the value of a Bernstein-Bezier polynomial.
// This method uses a Horner-like fast evaluation scheme.
// param t Time value
// param c_ Pointer to coefficients
// param n Degree of the polynomial (number of coefficients minus one)
//
double
BezierCurve::_bernstein_value_at(double t, double const * c_, unsigned n)
{
  double u = 1.0 - t;
  double bc = 1;
  double tn = 1;
  double tmp = c_[0] * u;

  for(unsigned i = 1; i < n; i++)
  {
    tn = tn * t;
    bc = bc * (n - i + 1) / i;
    tmp = (tmp + tn * bc * c_[i]) * u;
  }

  return (tmp + tn * t * c_[n]);
}

Eigen::Vector2d
BezierCurve::_bezier_value_at(const Eigen::Matrix2Xd & control_points, const double t)
{

  assert_break(t <= 1.);
  assert_break(t >= 0);

  const int order = (int)control_points.cols() - 1;
  Eigen::VectorXd control_x_vals = control_points.row(0);
  Eigen::VectorXd control_y_vals = control_points.row(1);

  Eigen::Vector2d ans = Eigen::Vector2d::Zero();
  ans(0) = _bernstein_value_at(t, control_x_vals.data(), order);
  ans(1) = _bernstein_value_at(t, control_y_vals.data(), order);

  return ans;
}

// From https://github.com/inkscape/lib2geom/: src/2geom/bezier.cpp
Eigen::Matrix2Xd
BezierCurve::_derivative_bezier_curve(const Eigen::Matrix2Xd & control_points)
{
  assert_break(control_points.cols() >= 2);
  Eigen::Matrix2Xd ans(2, control_points.cols() - 1);

  const int order = (int)control_points.cols() - 1;

  for(unsigned i = 0; i < ans.cols(); ++i)
  {
    ans.col(i) = order * (control_points.col(i + 1) - control_points.col(i));
  }

  return ans;
}

Eigen::Matrix3Xd
BezierCurve::_tesselate(const Eigen::Matrix2Xd & control_points, const int n_sampling)
{
  Eigen::Matrix3Xd ans(3, n_sampling);

  for(int i = 0; i < n_sampling; ++i)
  {
    const double t = 1. / (n_sampling - 1) * i;
    ans.col(i)(0) = t;
    ans.col(i).tail<2>() = _bezier_value_at(control_points, t);
  }

  return ans;
}



double
BezierCurve::length()
{
  const std::vector<double> & ww = _get_gauss_quad_weights();
  const std::vector<double> & tt = _get_gauss_quad_locs();
  double len = 0;

  for(unsigned i = 0; i < tt.size(); ++i)
  {
    len += ww[i] * dposdt(tt[i]).norm();
  }

  return len;
}


Eigen::VectorXd
BezierCurve::dlengthdparams()
{
  const int n_points = n_control_points();
  const double tol = 1e-10;
  const std::vector<double> & ww = _get_gauss_quad_weights();
  const std::vector<double> & tt = _get_gauss_quad_locs();

  Eigen::VectorXd dlendprams = Eigen::VectorXd(n_points * 2);
  dlendprams.setZero();

  for(unsigned i = 0; i < tt.size(); ++i)
  {
    Eigen::Vector2d tang = dposdt(tt[i]);
    double tang_norm = std::max(tang.norm(), tol);
    dlendprams += ww[i] * 1. / tang_norm * dposdtdparams(tt[i]).transpose() * tang;
  }

  return dlendprams;
}

const std::vector<double> &
BezierCurve::_get_gauss_quad_locs()
{
  static std::vector<double> ans;
  static bool is_init = false;

  if(!is_init)
  {
    ans.resize(6);
    ans[0] = -9.3246951420315202781230155449399e-01L;
    ans[1] = -6.6120938646626451366139959501991e-01L;
    ans[2] = -2.3861918608319690863050172168071e-01L;
    ans[3] = -ans[2];
    ans[4] = -ans[1];
    ans[5] = -ans[0];

    for(double & a : ans)
    {
      a = a / 2. + 0.5;
    }

    is_init = true;
  }

  return ans;
}

const std::vector<double> &
BezierCurve::_get_gauss_quad_weights()
{
  static std::vector<double> ans;
  static bool is_init = false;

  if(!is_init)
  {
    ans.resize(6);
    ans[0] = 1.7132449237917034504029614217273e-01L;
    ans[1] = 3.6076157304813860756983351383772e-01L;
    ans[2] = 4.6791393457269104738987034398955e-01L;
    ans[3] = ans[2];
    ans[4] = ans[1];
    ans[5] = ans[0];

    for(double & a : ans)
    {
      a = a / 2.;
    }

    is_init = true;
  }

  return ans;
}
