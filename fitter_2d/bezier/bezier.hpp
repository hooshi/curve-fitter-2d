#ifndef PIXVEC_BEZIER_FITTER_IS_INCLUDED
#define PIXVEC_BEZIER_FITTER_IS_INCLUDED

#include <functional>
#include <vector>

#include <Eigen/Core>


// =====================================================
//                    CURVE
// =====================================================
class BezierCurve
{
public:
  BezierCurve() = default;

  Eigen::Vector2d pos(const double t) const;
  Eigen::Vector2d dposdt(const double t) const;
  Eigen::Vector2d dposdtdt(const double t) const;

  Eigen::Matrix2Xd dposdparams(const double t) const;
  Eigen::Matrix2Xd dposdtdparams(const double t) const;

  double project(const Eigen::Vector2d & point) const;

  Eigen::VectorXd dtprojectdparams(const double t, const Eigen::Vector2d & point);
  Eigen::Matrix2Xd dposprojectdparams(const double t, const Eigen::VectorXd & dtprojectdparams);
  Eigen::Matrix2Xd dposdtprojectdparams(const double t, const Eigen::VectorXd & dtprojectdparams);

  void set_control_points(const Eigen::Matrix2Xd & control_points_d0_in);
  const Eigen::Matrix2Xd & get_control_points();
  constexpr static int n_control_points() { return 4; }

  const Eigen::Matrix3Xd & get_tesselation3();
  const Eigen::Ref<const Eigen::Matrix2Xd> get_tesselation2();

  double length();
  Eigen::VectorXd dlengthdparams();

private:
  constexpr static unsigned _n_tesselation = 50;

  static double _bernstein_value_at(double t, double const * c_, unsigned n);
  static Eigen::Vector2d _bezier_value_at(const Eigen::Matrix2Xd & _control_points, const double t);
  static Eigen::Matrix2Xd _derivative_bezier_curve(const Eigen::Matrix2Xd & _control_points);
  static Eigen::Matrix3Xd _tesselate(const Eigen::Matrix2Xd & _control_points, const int n_sampling);

  Eigen::Matrix2Xd _control_points_d0;
  Eigen::Matrix2Xd _control_points_d1;
  Eigen::Matrix2Xd _control_points_d2;
  Eigen::Matrix3Xd _tesselation;

  const static std::vector<double> & _get_gauss_quad_weights();
  const static std::vector<double> & _get_gauss_quad_locs();
};


#endif
