#include <unsupported/Eigen/NonLinearOptimization>

#include <fitter_2d/globfit/globfitter.hpp>

// ============= EIGEN INTERFACE FOR MARQUART_LEVENBERG

int
GlobFitter::inputs()
{
  return n_parameters();
}

int
GlobFitter::values()
{
  return n_equations();
}

int
GlobFitter::operator()(const Eigen::VectorXd & x, Eigen::VectorXd & fvec)
{
  const double tol = 1e-10;

  if((_cached_params.size() == 0) || ((_cached_params - x).norm() > tol))
  {
    set_params(x);
    _cached_params = x;
    compute_objective_and_jacobian(_cached_objective, _cached_jacobian);
  }

  fvec = _cached_objective;
  return 0;
}

int
GlobFitter::df(const Eigen::VectorXd & x, Eigen::MatrixXd & fjac)
{
  const double tol = 1e-10;

  if((_cached_params.size() == 0) || ((_cached_params - x).norm() > tol))
  {
    set_params(x);
    _cached_params = x;
    compute_objective_and_jacobian(_cached_objective, _cached_jacobian);
  }

  fjac = _cached_jacobian;
  return 0;
}

// Separate file as it takes ages to compile.
void
GlobFitter::run_fitter(bool verbose, std::function<void(int)> callback)
{
  assert_break_msg(_is_setup, "Must call setup() first");

  Eigen::VectorXd x0 = get_params();
  Eigen::VectorXd x = x0;

  // Init Eigen
  Eigen::LevenbergMarquardt<GlobFitter> lm(*this);
  using Eigen::LevenbergMarquardtSpace::Running;
  using Eigen::LevenbergMarquardtSpace::Status;

  // Init the jacobian and obj.
  compute_objective_and_jacobian(_cached_objective, _cached_jacobian);

  Status status = lm.minimizeInit(x);
  int iter_id = 0;

  if(verbose)
  {
    printf(" ==== MARQURT-LEVENBERG for Bezier Fitting ===== \n");
    printf(" %10s %20s %20s \n", "ITER", "OBJ", "GRAD");
    fflush(stdout);
  }

  bool keep_running = true;

  do
  {
    status = lm.minimizeOneStep(x);

    if(verbose)
    {
      if((iter_id % 10 == 0) || (status != Running))
      {
        printf(" %10d %20.8e %20.8e \n",
            iter_id,
            _cached_objective.norm(),
            (_cached_jacobian.transpose() * _cached_objective).norm());
        fflush(stdout);
      }
    }

    ++iter_id;
    callback(iter_id);

    if(status != Running)
    {
      keep_running = false;
    }
    else if(iter_id >= _max_iterations)
    {
      keep_running = false;
    }

  } while(keep_running);

  if(verbose)
  {
    switch(status)
    {
#define TAKECAREOF(XX) \
  case(Eigen::LevenbergMarquardtSpace::XX): printf("FINISHED, Status: %s \n", #XX); break

      TAKECAREOF(NotStarted);
      TAKECAREOF(Running);
      TAKECAREOF(ImproperInputParameters);
      TAKECAREOF(RelativeReductionTooSmall);
      TAKECAREOF(RelativeErrorTooSmall);
      TAKECAREOF(RelativeErrorAndReductionTooSmall);
      TAKECAREOF(CosinusTooSmall);
      TAKECAREOF(TooManyFunctionEvaluation);
      TAKECAREOF(FtolTooSmall);
      TAKECAREOF(XtolTooSmall);
      TAKECAREOF(GtolTooSmall);
      TAKECAREOF(UserAsked);

#undef TAKECAREOF
    }

    fflush(stdout);
  }


  set_params(x);
}
