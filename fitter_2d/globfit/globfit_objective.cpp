// Author: A very tired Shayan Hoshyari (Mr. Chain rule).

#include <fitter_2d/globfit/globfit_curve.hpp>
#include <fitter_2d/globfit/globfit_objective.hpp>

// debugging
#include <fitter_2d/bezier/bezier.hpp>

// =============================================================
//                           BASE CLASS
// =============================================================

const std::vector<int> &
GlobFitObjective::get_curve_ids() const
{
  assert_break(_curve_ids.size());
  return _curve_ids;
}

void
GlobFitObjective::set_weight(const double weight)
{
  assert_break(weight >= 0);
  _weight = weight;
  _sqrt_weight = sqrt(weight);
}

double
GlobFitObjective::get_weight()
{
  return _weight;
}

double
GlobFitObjective::get_sqrt_weight()
{
  return _sqrt_weight;
}

void
GlobFitObjective::set_curves(std::vector<GlobFitCurve *> curves_in)
{
  assert_break(curves_in.size() == _curve_ids.size());
  _cached_curves = curves_in;
}


void
GlobFitObjective::_forget_curves()
{
  _cached_curves.resize(0);
}

int
GlobFitObjective::_n_sum_curve_params()
{
  int ans = 0;

  for(GlobFitCurve * c : _cached_curves)
  {
    ans += c->n_params();
  }

  return ans;
}


bool
GlobFitObjective::_is_all_data_provided()
{
  bool answer = true;

  answer = answer && (_curve_ids.size());
  answer = answer && (_cached_curves.size() == _curve_ids.size());
  answer = answer && (_weight >= 0);

  return answer;
}

// =============================================================
//                           FIT POINTS
// =============================================================

void
GlobFitObjective_FitPoints::set_points_to_fit(PointWeightFeeder feeder, const int n_points)
{
  _point_feeder = feeder;
  _n_points_to_fit = n_points;
}

void
GlobFitObjective_FitPoints::get_points_to_fit(PointWeightFeeder & feeder, int & n_points) const
{
  feeder = _point_feeder;
  n_points = _n_points_to_fit;
}

int
GlobFitObjective_FitPoints::n_equations()
{
  return int(_n_points_to_fit) * 2;
}

GlobFitObjectiveType
GlobFitObjective_FitPoints::get_type()
{
  return GLOBFIT_OBJECTIVE_FIT_POINTS;
}

void
GlobFitObjective_FitPoints::compute_objective_and_jacobian(Eigen::VectorXd & obj, Eigen::MatrixXd & dobj_dcurveparams)
{

  assert_break_msg(_is_all_data_provided(), "Some data is not provided yet or is wrong");

  dobj_dcurveparams.setZero(n_equations(), _n_sum_curve_params());
  obj.setZero(n_equations());

  GlobFitCurve & curve = *_cached_curves.front();

  //
  // Points to fit
  //
  for(int ptid = 0; ptid < (int)_n_points_to_fit; ++ptid)
  {

    Eigen::Vector2d point_to_fit;
    double weight_to_fit, sqrt_weight_to_fit;
    _point_feeder(ptid, point_to_fit, weight_to_fit);
    sqrt_weight_to_fit = sqrt(weight_to_fit);

    const double t_proj = curve.project(point_to_fit);
    Eigen::Vector2d projection = curve.pos(t_proj);
    const Eigen::VectorXd dtprojdparams = curve.dtprojectdparams(t_proj, point_to_fit);
    const Eigen::Matrix2Xd dpointprojdparams = curve.dposprojectdparams(t_proj, dtprojdparams);

    const Eigen::Vector2d distance_vector = projection - point_to_fit;

    obj.segment<2>(2 * ptid) = sqrt_weight_to_fit * distance_vector;
    dobj_dcurveparams.middleRows<2>(2 * ptid) = sqrt_weight_to_fit * dpointprojdparams;
  }

  // force user to call set_curves() each time;
  _forget_curves();
}

bool
GlobFitObjective_FitPoints::_is_all_data_provided()
{
  bool answer = true;
  answer = answer && GlobFitObjective::_is_all_data_provided();
  answer = answer && (_cached_curves.size() == 1);
  answer = answer && (_n_points_to_fit > 0);
  answer = answer && (_point_feeder);
  return answer;
}

// =============================================================
//                           FIT POINTS
// =============================================================

void
GlobFitObjective_FitTangents::set_points_to_fit(PointTangentWeightFeeder feeder, const int n_points)
{
  _tangent_feeder = feeder;
  _n_points_to_fit = n_points;
}

void
GlobFitObjective_FitTangents::get_points_to_fit(PointTangentWeightFeeder & feeder, int & n_points) const
{
  feeder = _tangent_feeder;
  n_points = _n_points_to_fit;
}

int
GlobFitObjective_FitTangents::n_equations()
{ //
  return _n_points_to_fit * 2;
}

GlobFitObjectiveType
GlobFitObjective_FitTangents::get_type()
{ //
  return GLOBFIT_OBJECTIVE_FIT_TANGENTS;
}

void
GlobFitObjective_FitTangents::compute_objective_and_jacobian(Eigen::VectorXd & obj, Eigen::MatrixXd & jac)
{

  assert_break_msg(_is_all_data_provided(), "Some data is not provided yet or is wrong");

  jac.setZero(n_equations(), _n_sum_curve_params());
  obj.setZero(n_equations());

  GlobFitCurve & curve = *_cached_curves.front();

  //
  // Points to fit
  //
  for(int ptid = 0; ptid < (int)_n_points_to_fit; ++ptid)
  {

    Eigen::Vector2d point_to_fit, tangent_to_fit;
    double weight_to_fit, sqrt_weight_to_fit;
    _tangent_feeder(ptid, point_to_fit, tangent_to_fit, weight_to_fit);
    sqrt_weight_to_fit = sqrt(weight_to_fit);
    tangent_to_fit.normalize();

    const double t_proj = curve.project(point_to_fit);
    Eigen::Vector2d dposdt = curve.dposdt(t_proj);
    const Eigen::VectorXd dtdparams = curve.dtprojectdparams(t_proj, point_to_fit);
    const Eigen::Matrix2Xd dposdtdbezierparams = curve.dposdtprojectdparams(t_proj, dtdparams);

    const double tol = 1e-15;
    double dposdt_norm2 = std::max(dposdt.squaredNorm(), tol);
    double dposdt_norm = sqrt(dposdt_norm2);

#if 1
    // Actual tangent and its derivative -- must normalizer derivative
    const Eigen::Vector2d tang = dposdt / dposdt_norm;
    const Eigen::Matrix2d dtang_ddposdt =
        Eigen::Matrix2d::Identity() / dposdt_norm - 1. / (dposdt_norm * dposdt_norm2) * dposdt * dposdt.transpose();
#else
    // just for testing -- don't normalize
    param_unused(tol);
    param_unused(dposdt_norm2);
    param_unused(dposdt_norm);
    const Eigen::Vector2d tang = dposdt;
    const Eigen::Matrix2d dtang_ddposdt = Eigen::Matrix2d::Identity();
#endif
    const Eigen::Vector2d diff = tang - tangent_to_fit;

    // Chain rule is your friend.
    obj.segment<2>(2 * ptid) = sqrt_weight_to_fit * diff;
    jac.middleRows<2>(2 * ptid) = sqrt_weight_to_fit * dtang_ddposdt * dposdtdbezierparams;
  }

  // force user to call set_curves() each time;
  _forget_curves();
}


bool
GlobFitObjective_FitTangents::_is_all_data_provided()
{
  bool answer = true;
  answer = answer && GlobFitObjective::_is_all_data_provided();
  answer = answer && (_cached_curves.size() == 1);
  answer = answer && (_n_points_to_fit > 0);
  answer = answer && (_tangent_feeder);
  return answer;
}

// =============================================================
//                          FIX POSITION
// =============================================================

void
GlobFitObjective_FixPosition::set_t_for_fixed_position(const double t_curve)
{
  _t_curve = t_curve;
}

void
GlobFitObjective_FixPosition::set_fixed_position(const Eigen::Vector2d & pt)
{
  _fixed_pos = pt;
}

int
GlobFitObjective_FixPosition::n_equations()
{
  return 2;
}

GlobFitObjectiveType
GlobFitObjective_FixPosition::get_type()
{
  return GLOBFIT_OBJECTIVE_FIX_POSITION;
}

void
GlobFitObjective_FixPosition::compute_objective_and_jacobian(Eigen::VectorXd & obj, Eigen::MatrixXd & dobj_dcurveparams)
{

  assert_break_msg(_is_all_data_provided(), "Some data is not provided yet or is wrong");

  dobj_dcurveparams.resize(n_equations(), _n_sum_curve_params());
  obj.resize(n_equations());

  GlobFitCurve & curve0 = *_cached_curves.front();
  Eigen::Vector2d pos_curve0 = curve0.pos(_t_curve);

  obj = (pos_curve0 - _fixed_pos);
  dobj_dcurveparams = curve0.dposdparams(_t_curve);

  // force user to call set_curves() each time;
  _forget_curves();
}

bool
GlobFitObjective_FixPosition::_is_all_data_provided()
{
  bool answer = true;
  const double tol = 1e-12;
  answer = answer && GlobFitObjective::_is_all_data_provided();
  answer = answer && (_cached_curves.size() == 1);
  answer = answer && (_t_curve > 0 - tol);
  answer = answer && (_t_curve < GlobFitCurve::t_end + tol);
  answer = answer && (_fixed_pos.norm() < std::numeric_limits<double>::max());
  return answer;
}

// =============================================================
//                          FIX TANGENT
// =============================================================

void
GlobFitObjective_FixTangent::set_t_for_fixed_tangent(const double t_curve)
{
  _t_curve = t_curve;
}

void
GlobFitObjective_FixTangent::set_fixed_tangent(const Eigen::Vector2d & tng)
{
  _fixed_tangent = tng;
}

int
GlobFitObjective_FixTangent::n_equations()
{
  return 1;
}

GlobFitObjectiveType
GlobFitObjective_FixTangent::get_type()
{
  return GLOBFIT_OBJECTIVE_FIX_TANGENT;
}

void
GlobFitObjective_FixTangent::compute_objective_and_jacobian(Eigen::VectorXd & obj, Eigen::MatrixXd & dobj_dcurveparams)
{

  assert_break_msg(_is_all_data_provided(), "Some data is not provided yet or is wrong");

  dobj_dcurveparams.resize(n_equations(), _n_sum_curve_params());
  obj.resize(n_equations());

  GlobFitCurve & curve0 = *_cached_curves.front();

  Eigen::Vector2d dposdt_curve0 = curve0.dposdt(_t_curve);

  obj(0) = (dposdt_curve0.x() * _fixed_tangent.y() - dposdt_curve0.y() * _fixed_tangent.x());

  Eigen::RowVector2d dobj_ddposdt_curve0;
  dobj_ddposdt_curve0 << _fixed_tangent.y(), -_fixed_tangent.x();

  dobj_dcurveparams = dobj_ddposdt_curve0 * curve0.dposdtdparams(_t_curve);

  // force user to call set_curves() each time;
  _forget_curves();
}

bool
GlobFitObjective_FixTangent::_is_all_data_provided()
{
  bool answer = true;
  const double tol = 1e-12;
  answer = answer && GlobFitObjective::_is_all_data_provided();
  answer = answer && (_cached_curves.size() == 1);
  answer = answer && (_t_curve > 0 - tol);
  answer = answer && (_t_curve < GlobFitCurve::t_end + tol);
  answer = answer && (_fixed_tangent.norm() < std::numeric_limits<double>::max());
  return answer;
}

// =============================================================
//                          SAME POSITION
// =============================================================

void
GlobFitObjective_SamePosition::set_t_for_same_position(const double t_curve0, const double t_curve1)
{
  _t_curve0 = t_curve0;
  _t_curve1 = t_curve1;
}

void
GlobFitObjective_SamePosition::get_t_for_same_position(double & t_curve0, double & t_curve1)
{
  t_curve0 = _t_curve0;
  t_curve1 = _t_curve1;
}

int
GlobFitObjective_SamePosition::n_equations()
{
  return 2;
}

GlobFitObjectiveType
GlobFitObjective_SamePosition::get_type()
{
  return GLOBFIT_OBJECTIVE_SAME_POSITION;
}

void
GlobFitObjective_SamePosition::compute_objective_and_jacobian(Eigen::VectorXd & obj,
    Eigen::MatrixXd & dobj_dcurveparams)
{

  assert_break_msg(_is_all_data_provided(), "Some data is not provided yet or is wrong");

  dobj_dcurveparams.resize(n_equations(), _n_sum_curve_params());
  obj.resize(n_equations());

  GlobFitCurve & curve0 = *_cached_curves.front();
  GlobFitCurve & curve1 = *_cached_curves.back();

  Eigen::Vector2d pos_curve0 = curve0.pos(_t_curve0);
  Eigen::Vector2d pos_curve1 = curve1.pos(_t_curve1);

  obj = (pos_curve0 - pos_curve1);
  dobj_dcurveparams.leftCols(curve0.n_params()) = curve0.dposdparams(_t_curve0);
  dobj_dcurveparams.rightCols(curve1.n_params()) = -curve1.dposdparams(_t_curve1);

  // force user to call set_curves() each time;
  _forget_curves();
}

bool
GlobFitObjective_SamePosition::_is_all_data_provided()
{
  bool answer = true;
  const double tol = 1e-12;
  answer = answer && GlobFitObjective::_is_all_data_provided();
  answer = answer && (_cached_curves.size() == 2);
  answer = answer && (_t_curve0 > 0 - tol);
  answer = answer && (_t_curve1 > 0 - tol);
  answer = answer && (_t_curve0 < GlobFitCurve::t_end + tol);
  answer = answer && (_t_curve1 < GlobFitCurve::t_end + tol);
  return answer;
}

// =============================================================
//                          SAME TANGENT
// =============================================================

void
GlobFitObjective_SameTangent::set_t_for_same_position(const double t_curve0, const double t_curve1)
{
  _t_curve0 = t_curve0;
  _t_curve1 = t_curve1;
}

void
GlobFitObjective_SameTangent::get_t_for_same_position(double & t_curve0, double & t_curve1)
{
  t_curve0 = _t_curve0;
  t_curve1 = _t_curve1;
}

int
GlobFitObjective_SameTangent::n_equations()
{
  return 1;
}

GlobFitObjectiveType
GlobFitObjective_SameTangent::get_type()
{
  return GLOBFIT_OBJECTIVE_SAME_TANGENT;
}

void
GlobFitObjective_SameTangent::compute_objective_and_jacobian(Eigen::VectorXd & obj, Eigen::MatrixXd & dobj_dcurveparams)
{

  assert_break_msg(_is_all_data_provided(), "Some data is not provided yet or is wrong");

  dobj_dcurveparams.resize(n_equations(), _n_sum_curve_params());
  obj.resize(n_equations());

  GlobFitCurve & curve0 = *_cached_curves.front();
  GlobFitCurve & curve1 = *_cached_curves.back();

  Eigen::Vector2d dposdt_curve0 = curve0.dposdt(_t_curve0);
  Eigen::Vector2d dposdt_curve1 = curve1.dposdt(_t_curve1);

  obj(0) = (dposdt_curve0.x() * dposdt_curve1.y() - dposdt_curve0.y() * dposdt_curve1.x());

  Eigen::RowVector2d dobj_ddposdt_curve0;
  Eigen::RowVector2d dobj_ddposdt_curve1;
  dobj_ddposdt_curve0 << dposdt_curve1.y(), -dposdt_curve1.x();
  dobj_ddposdt_curve1 << -dposdt_curve0.y(), dposdt_curve0.x();

  dobj_dcurveparams.leftCols(curve0.n_params()) = dobj_ddposdt_curve0 * curve0.dposdtdparams(_t_curve0);
  dobj_dcurveparams.rightCols(curve1.n_params()) = dobj_ddposdt_curve1 * curve1.dposdtdparams(_t_curve1);

  // force user to call set_curves() each time;
  _forget_curves();
}

bool
GlobFitObjective_SameTangent::_is_all_data_provided()
{
  bool answer = true;
  const double tol = 1e-12;
  answer = answer && GlobFitObjective::_is_all_data_provided();
  answer = answer && (_cached_curves.size() == 2);
  answer = answer && (_t_curve0 > 0 - tol);
  answer = answer && (_t_curve1 > 0 - tol);
  answer = answer && (_t_curve0 < GlobFitCurve::t_end + tol);
  answer = answer && (_t_curve1 < GlobFitCurve::t_end + tol);
  return answer;
}


// =============================================================
//                BEZIER SMALL DEVIATION FROM INIT
// =============================================================

void
GlobFitObjective_BezierFairness::set_initial_bezier(const Eigen::Matrix2Xd & control_points)
{
  BezierCurve bz;
  bz.set_control_points(control_points);
  _initial_bezier.set_bezier(bz);
  _is_initial_bezier_set = true;
}

void
GlobFitObjective_BezierFairness::set_sub_weights(const double weight_end_tangent_length_dev,
    const double weight_end_tangent_angle_dev,
    const double weight_end_points_dev,
    const double weight_length,
    const double weight_prevent_control_point_overlap,
    const double thr_prevent_control_point_overlap)
{
  _weight_end_tangent_length_dev = weight_end_tangent_length_dev;
  _weight_end_tangent_angle_dev = weight_end_tangent_angle_dev;
  _weight_length = weight_length;
  _weight_end_points_dev = weight_end_points_dev;
  _weight_prevent_control_point_overlap = weight_prevent_control_point_overlap;
  _thr_prevent_control_point_overlap = thr_prevent_control_point_overlap;
}


int
GlobFitObjective_BezierFairness::n_equations()
{
  return 11;
}


GlobFitObjectiveType
GlobFitObjective_BezierFairness::get_type()
{
  return GLOBFIT_OBJECTIVE_BEZIER_FAIRNESS;
}

void
GlobFitObjective_BezierFairness::compute_objective_and_jacobian(Eigen::VectorXd & obj,
    Eigen::MatrixXd & dobj_dcurveparams)
{

  assert_break_msg(_is_all_data_provided(), "Some data is not provided yet or is wrong");

  dobj_dcurveparams.resize(n_equations(), _n_sum_curve_params());
  obj.resize(n_equations());

  GlobFitCurve & curve = *_cached_curves.front();
  assert_break(curve.get_type() == GLOBFIT_CURVE_BEZIER);
  GlobFitCurve_Bezier & bezier = *static_cast<GlobFitCurve_Bezier *>(&curve);
  Eigen::VectorXd bezier_params = bezier.get_params();
  Eigen::VectorXd initial_params = _initial_bezier.get_params();


  // We must know the index mapping before hand.
  const int idx_l0 = 0;
  const int idx_l1 = 1;
  const int idx_pt0 = 2;
  const int idx_pt1 = 4;
  const int idx_angle0 = 6;
  const int idx_angle1 = 7;

  // Deviation of distance between control points
  obj(0) = _weight_end_tangent_length_dev * (bezier_params(idx_l0) - initial_params(idx_l0));
  obj(1) = _weight_end_tangent_length_dev * (bezier_params(idx_l1) - initial_params(idx_l1));
  // Deviation of begin and end angles
  obj(2) = _weight_end_tangent_angle_dev * (bezier_params(idx_angle0) - initial_params(idx_angle0));
  obj(3) = _weight_end_tangent_angle_dev * (bezier_params(idx_angle1) - initial_params(idx_angle1));
  // Deviation of begin and end control points
  obj.segment<2>(4) = _weight_end_points_dev * (bezier_params.segment<2>(idx_pt0) - initial_params.segment<2>(idx_pt0));
  obj.segment<2>(6) = _weight_end_points_dev * (bezier_params.segment<2>(idx_pt1) - initial_params.segment<2>(idx_pt1));
  // Regularizing length
  obj(8) = _weight_length * (bezier.length() - _initial_bezier.length());

  // Don't let control points collapse on each other
  obj(9) = 0;
  obj(10) = 0;

  if(bezier_params(idx_l0) < _thr_prevent_control_point_overlap)
  {
    obj(9) = _thr_prevent_control_point_overlap - bezier_params(idx_l0);
  }

  if(bezier_params(idx_l1) < _thr_prevent_control_point_overlap)
  {
    obj(10) = _thr_prevent_control_point_overlap - bezier_params(idx_l1);
  }

  // Now derivatives
  dobj_dcurveparams.setZero();
  dobj_dcurveparams(0, idx_l0) = _weight_end_tangent_length_dev;
  dobj_dcurveparams(1, idx_l1) = _weight_end_tangent_length_dev;
  dobj_dcurveparams(2, idx_angle0) = _weight_end_tangent_angle_dev;
  dobj_dcurveparams(3, idx_angle1) = _weight_end_tangent_angle_dev;
  dobj_dcurveparams(4, idx_pt0 + 0) = _weight_end_points_dev;
  dobj_dcurveparams(5, idx_pt0 + 1) = _weight_end_points_dev;
  dobj_dcurveparams(6, idx_pt1 + 0) = _weight_end_points_dev;
  dobj_dcurveparams(7, idx_pt1 + 1) = _weight_end_points_dev;
  dobj_dcurveparams.row(8) = _weight_length * bezier.dlengthdparams();

  if(bezier_params(idx_l0) < _thr_prevent_control_point_overlap)
  {
    obj(9, idx_l0) = -1;
  }

  if(bezier_params(idx_l1) < _thr_prevent_control_point_overlap)
  {
    obj(10, idx_l1) = -1;
  }

  // force user to call set_curves() each time;
  _forget_curves();
}

bool
GlobFitObjective_BezierFairness::_is_all_data_provided()
{
  bool answer = true;
  answer = answer && GlobFitObjective::_is_all_data_provided();
  answer = answer && (_weight_end_tangent_length_dev >= 0.);
  answer = answer && (_weight_end_tangent_angle_dev >= 0.);
  answer = answer && (_weight_length >= 0.);
  answer = answer && (_weight_end_points_dev >= 0.);
  answer = answer && (_weight_prevent_control_point_overlap >= 0.);
  answer = answer && (_thr_prevent_control_point_overlap > 0.);
  answer = answer && (_is_initial_bezier_set);
  return answer;
}
