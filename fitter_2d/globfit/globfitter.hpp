#ifndef DEADLINE_CODE_GLOBFITTER_IS_INCLUDED
#define DEADLINE_CODE_GLOBFITTER_IS_INCLUDED

#include <memory>
#include <vector>

#include <Eigen/Core>

#include <fitter_2d/share/util.hpp>


class GlobFitObjective;
class GlobFitCurve;

struct GlobFitter
{
public:
  void set_curves(const std::vector<GlobFitCurve *> &);
  void set_objectives(const std::vector<GlobFitObjective *> &);
  void setup(const int max_iterations);
  void run_fitter(bool verbose, std::function<void(int)> callback);

  int n_parameters();
  int n_equations();
  bool is_setup();

  std::vector<GlobFitCurve *> get_curves();
  std::vector<GlobFitObjective *> get_objectives();

  void compute_objective_and_jacobian(Eigen::VectorXd & obj, Eigen::MatrixXd & dobj_dcurveparams);
  Eigen::VectorXd get_params();
  void set_params(const Eigen::VectorXd &);

  GlobFitter() = default;

private:
  std::vector<GlobFitCurve *> _curves = std::vector<GlobFitCurve *>();
  std::vector<GlobFitObjective *> _objectives = std::vector<GlobFitObjective *>();
  bool _is_setup = false;
  std::vector<int> _xadj_equations = std::vector<int>();
  std::vector<int> _xadj_parameters = std::vector<int>();
  int _max_iterations;

  Eigen::VectorXd _cached_params;
  Eigen::VectorXd _cached_objective;
  Eigen::MatrixXd _cached_jacobian;

  // ============== API needed by Eigen Levenberg-Marquart
public:
  enum
  {
    InputsAtCompileTime = Eigen::Dynamic,
    ValuesAtCompileTime = Eigen::Dynamic
  };
  typedef Eigen::VectorXd InputType;
  typedef Eigen::VectorXd ValueType;
  typedef Eigen::MatrixXd JacobianType;

  int inputs();
  int values();
  int operator()(const Eigen::VectorXd & x, Eigen::VectorXd & fvec);
  int df(const Eigen::VectorXd & x, Eigen::MatrixXd & fjac);
};


#endif // DEADLINE_CODE_GLOBFITTER_IS_INCLUDED
