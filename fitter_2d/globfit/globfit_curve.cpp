// Author: A tired Shayan Hoshyari.

// Allow building without the arc. On my Linux laptop I cannot access api/*.
#undef WITH_ARC
#ifdef WITH_ARC
#include <api/geom.hpp>
#endif

#include <fitter_2d/bezier/bezier.hpp>
#include <fitter_2d/globfit/globfit_curve.hpp>
#include <fitter_2d/share/projection_derivative.hpp>
#include <fitter_2d/share/util.hpp>



constexpr double GlobFitCurve::t_end;

// =============================================================
//                             Line
// =============================================================
constexpr int GlobFitCurve_Line::_n_params;

GlobFitCurveType
GlobFitCurve_Line::get_type()
{
  return GLOBFIT_CURVE_LINE;
}

int
GlobFitCurve_Line::n_params()
{
  return _n_params;
}

Eigen::Vector2d
GlobFitCurve_Line::pos(const double t)
{
  assert_break(_are_points_set);
  assert_break(t <= GlobFitCurve::t_end);
  assert_break(t >= 0);
  return _points.col(0) + (_points.col(1) - _points.col(0)) * t;
}

Eigen::Vector2d
GlobFitCurve_Line::dposdt(const double t)
{
  assert_break(_are_points_set);
  assert_break(t <= GlobFitCurve::t_end);
  assert_break(t >= 0);
  return (_points.col(1) - _points.col(0));
}

Eigen::Vector2d
GlobFitCurve_Line::dposdtdt(const double t)
{
  assert_break(_are_points_set);
  assert_break(t <= GlobFitCurve::t_end);
  assert_break(t >= 0);
  return Eigen::Vector2d::Zero();
}

Eigen::Matrix2Xd
GlobFitCurve_Line::dposdparams(const double t)
{
  assert_break(_are_points_set);
  assert_break(t <= GlobFitCurve::t_end);
  assert_break(t >= 0);
  Eigen::Matrix<double, 2, _n_params> ans;
  ans.row(0) << 1 - t, 0, t, 0;
  ans.row(1) << 0, 1 - t, 0, t;
  return ans;
}

Eigen::Matrix2Xd
GlobFitCurve_Line::dposdtdparams(const double t)
{
  assert_break(_are_points_set);
  assert_break(t <= GlobFitCurve::t_end);
  assert_break(t >= 0);
  Eigen::Matrix<double, 2, _n_params> ans;
  ans.row(0) << -1, 0, 1, 0;
  ans.row(1) << 0, -1, 0, 1;
  return ans;
}

double
GlobFitCurve_Line::project(const Eigen::Vector2d & point)
{
  assert_break(_are_points_set);
  const Eigen::Vector2d dir = (_points.col(1) - _points.col(0));
  const double boundless_t = dir.dot(point - _points.col(0)) / dir.squaredNorm();
  return std::min(t_end, std::max(0., boundless_t));
}

Eigen::VectorXd
GlobFitCurve_Line::dtprojectdparams(const double time, const Eigen::Vector2d & point)
{

  return ProjectionDerivative::eval(_n_params,
      time,
      GlobFitCurve::t_end,
      point,
      pos(time),
      dposdt(time),
      dposdtdt(time),
      dposdparams(time),
      dposdtdparams(time));
}

Eigen::Matrix2Xd
GlobFitCurve_Line::dposprojectdparams(const double t, const Eigen::VectorXd & dtprojectdparams)
{
  return dposdparams(t) + dposdt(t) * dtprojectdparams.transpose();
}

Eigen::Matrix2Xd
GlobFitCurve_Line::dposdtprojectdparams(const double t, const Eigen::VectorXd & dtprojectdparams)
{
  return dposdtdparams(t) + dposdtdt(t) * dtprojectdparams.transpose();
}


void
GlobFitCurve_Line::set_params(const Eigen::VectorXd & params)
{
  _are_points_set = true;
  _points = reshaped(params, 2, 2);
}

Eigen::VectorXd
GlobFitCurve_Line::get_params()
{
  return reshaped(_points, 4, 1);
}

Eigen::Matrix2Xd
GlobFitCurve_Line::get_tesselation2()
{
  return _points;
}

void
GlobFitCurve_Line::set_points(const Eigen::Matrix2d & points)
{
  _are_points_set = true;
  _points = points;
}

Eigen::Matrix2d
GlobFitCurve_Line::get_points()
{
  return _points;
}


// =============================================================
//                             ARC
// =============================================================
constexpr int GlobFitCurve_Arc::_n_params;

#if !defined(WITH_ARC)
struct Arc
{
};
#endif

GlobFitCurveType
GlobFitCurve_Arc::get_type()
{
  assert_break(_is_arc_set);
  return GLOBFIT_CURVE_ARC;
}

int
GlobFitCurve_Arc::GlobFitCurve_Arc::n_params()
{
  assert_break(_is_arc_set);
  return _n_params;
}

Eigen::Vector2d
GlobFitCurve_Arc::pos(const double t)
{
  assert_break(_is_arc_set);
#if defined(WITH_ARC)
  const double s = t * _arc_impl->length();
  return _arc_impl->pos(s);
#else
  param_unused(t);
  return Eigen::Vector2d::Constant(std::numeric_limits<double>::max());
#endif
}

Eigen::Vector2d
GlobFitCurve_Arc::dposdt(const double t)
{
  assert_break(_is_arc_set);
#if defined(WITH_ARC)
  const double s = t * _arc_impl->length();
  const double dsdt = _arc_impl->length();
  return dsdt * _arc_impl->der(s);
#else
  param_unused(t);
  return Eigen::Vector2d::Constant(std::numeric_limits<double>::max());
#endif
}

Eigen::Vector2d
GlobFitCurve_Arc::dposdtdt(const double t)
{
  assert_break(_is_arc_set);
#if defined(WITH_ARC)
  const double s = t * _arc_impl->length();
  const double dsdt = _arc_impl->length();
  return dsdt * dsdt * _arc_impl->der2(s);
#else
  param_unused(t);
  return Eigen::Vector2d::Constant(std::numeric_limits<double>::max());
#endif
}

Eigen::Matrix2Xd
GlobFitCurve_Arc::dposdparams(const double t)
{
  assert_break(_is_arc_set);
#if defined(WITH_ARC)
  const double s = t * _arc_impl->length();
  const Eigen::Vector2d dposds = _arc_impl->der(s);

  CurveParams::ParamDer2 dposdcornuparams, garbage;
  _arc_impl->derivative_at(s, dposdcornuparams, garbage);

  Eigen::Matrix2Xd drdparams(2, n_params());

  drdparams.col(CurveParams::X) = dposdcornuparams.col(CurveParams::X);
  drdparams.col(CurveParams::Y) = dposdcornuparams.col(CurveParams::Y);
  drdparams.col(CurveParams::ANGLE) = dposdcornuparams.col(CurveParams::ANGLE);
  drdparams.col(CurveParams::LENGTH) = t * dposds + dposdcornuparams.col(CurveParams::LENGTH);
  drdparams.col(CurveParams::CURVATURE) = dposdcornuparams.col(CurveParams::CURVATURE);

  return drdparams;
#else
  param_unused(t);
  return Eigen::Matrix2Xd::Constant(2, n_params(), std::numeric_limits<double>::max());
#endif
}

Eigen::Matrix2Xd
GlobFitCurve_Arc::dposdtdparams(const double t)
{
  assert_break(_is_arc_set);
#if defined(WITH_ARC)
  const double s = t * _arc_impl->length();
  const double dsdt = _arc_impl->length();
  const Eigen::Vector2d dposds = _arc_impl->der(s);
  const Eigen::Vector2d dposdsds = _arc_impl->der2(s);

  CurveParams::ParamDer2 garbage, drdparamds;
  _arc_impl->derivative_at(s, garbage, drdparamds);

  Eigen::Matrix2Xd drdparamdt(2, n_params());

  drdparamdt.col(CurveParams::X) = drdparamds.col(CurveParams::X) * dsdt;
  drdparamdt.col(CurveParams::Y) = drdparamds.col(CurveParams::Y) * dsdt;
  drdparamdt.col(CurveParams::ANGLE) = drdparamds.col(CurveParams::ANGLE) * dsdt;
  // Mr chain rule presents:
  drdparamdt.col(CurveParams::LENGTH) = dposds + drdparamds.col(CurveParams::LENGTH) * dsdt + dsdt * t * dposdsds;
  //
  drdparamdt.col(CurveParams::CURVATURE) = drdparamds.col(CurveParams::CURVATURE) * dsdt;

  return drdparamdt;
#else
  param_unused(t);
  return Eigen::Matrix2Xd::Constant(2, n_params(), std::numeric_limits<double>::max());
#endif
}

double
GlobFitCurve_Arc::project(const Eigen::Vector2d & point)
{
  assert_break(_is_arc_set);
#if defined(WITH_ARC)
  const double s = _arc_impl->project(point);
  const double t = s / _arc_impl->length();
  return t;
#else
  param_unused(point);
  return std::numeric_limits<double>::max();
#endif
}

Eigen::VectorXd
GlobFitCurve_Arc::dtprojectdparams(const double time, const Eigen::Vector2d & point)
{
  assert_break(_is_arc_set);
#if defined(WITH_ARC)
  return ProjectionDerivative::eval(_n_params,
      time,
      GlobFitCurve::t_end,
      point,
      pos(time),
      dposdt(time),
      dposdtdt(time),
      dposdparams(time),
      dposdtdparams(time));
#else
  param_unused(point);
  param_unused(time);
  return Eigen::VectorXd::Constant(n_params(), std::numeric_limits<double>::max());
#endif
}

Eigen::Matrix2Xd
GlobFitCurve_Arc::dposprojectdparams(const double t, const Eigen::VectorXd & dtprojectdparams)
{
  assert_break(_is_arc_set);
#if defined(WITH_ARC)
  return dposdparams(t) + dposdt(t) * dtprojectdparams.transpose();
#else
  param_unused(dtprojectdparams);
  param_unused(t);
  return dposdparams(t) + dposdt(t) * dtprojectdparams.transpose();
#endif
}

Eigen::Matrix2Xd
GlobFitCurve_Arc::dposdtprojectdparams(const double t, const Eigen::VectorXd & dtprojectdparams)
{
  assert_break(_is_arc_set);
#if defined(WITH_ARC)
  return dposdtdparams(t) + dposdtdt(t) * dtprojectdparams.transpose();
#else
  param_unused(dtprojectdparams);
  param_unused(t);
  return dposdtdparams(t) + dposdtdt(t) * dtprojectdparams.transpose();
#endif
}


void
GlobFitCurve_Arc::set_params(const Eigen::VectorXd & params)
{
  assert_break(_is_arc_set);
#if defined(WITH_ARC)
  _arc_impl->set_params(params);
#else
  param_unused(params);
#endif
}

Eigen::VectorXd
GlobFitCurve_Arc::get_params()
{
  assert_break(_is_arc_set);
#if defined(WITH_ARC)
  return _arc_impl->params();
#else
  return Eigen::VectorXd::Constant(n_params(), std::numeric_limits<double>::max());
#endif
}

Eigen::Matrix2Xd
GlobFitCurve_Arc::get_tesselation2()
{
  assert_break(_is_arc_set);
#if defined(WITH_ARC)
  const int n_tesselation = 75;
  Eigen::Matrix2Xd ans(2, n_tesselation);

  for(int i = 0; i < n_tesselation; ++i)
  {
    const double t = i * t_end / (n_tesselation - 1);
    ans.col(i) = pos(t);
  }

  return ans;
#else
  return Eigen::Matrix2Xd::Constant(2, 1, std::numeric_limits<double>::max());
#endif
}

void
GlobFitCurve_Arc::set_cornu_arc(Arc & curnu_arc)
{
#if defined(WITH_ARC)
  _arc_impl.reset((Arc *)curnu_arc.clone());
  _is_arc_set = true;
#else
  param_unused(curnu_arc);
#endif
}

Arc &
GlobFitCurve_Arc::get_cornu_arc()
{
  assert_break(_is_arc_set);
  return *_arc_impl.get();
}


// =============================================================
//                             BEZIER
// =============================================================
constexpr int GlobFitCurve_Bezier::_n_params;

// GlobFitCurve Curve interface
GlobFitCurveType
GlobFitCurve_Bezier::get_type()
{
  return GLOBFIT_CURVE_BEZIER;
}

int
GlobFitCurve_Bezier::n_params()
{
  return _n_params;
}

Eigen::Vector2d
GlobFitCurve_Bezier::pos(const double t)
{
  assert_break(_is_bezier_set);
  return _bezier_impl->pos(t);
}

Eigen::Vector2d
GlobFitCurve_Bezier::dposdt(const double t)
{
  assert_break(_is_bezier_set);
  return _bezier_impl->dposdt(t);
}

Eigen::Vector2d
GlobFitCurve_Bezier::dposdtdt(const double t)
{
  assert_break(_is_bezier_set);
  return _bezier_impl->dposdtdt(t);
}

Eigen::Matrix2Xd
GlobFitCurve_Bezier::dposdparams(const double t)
{
  assert_break(_is_bezier_set);
  return _bezier_impl->dposdparams(t) * _dbezierpts_dfittingparams;
}

Eigen::Matrix2Xd
GlobFitCurve_Bezier::dposdtdparams(const double t)
{
  assert_break(_is_bezier_set);
  return _bezier_impl->dposdtdparams(t) * _dbezierpts_dfittingparams;
}

double
GlobFitCurve_Bezier::project(const Eigen::Vector2d & point)
{
  assert_break(_is_bezier_set);
  return _bezier_impl->project(point);
}

Eigen::VectorXd
GlobFitCurve_Bezier::dtprojectdparams(const double t, const Eigen::Vector2d & point)
{
  // this one is inverted as well, column rather than row
  assert_break(_is_bezier_set);
  return _dbezierpts_dfittingparams.transpose() * _bezier_impl->dtprojectdparams(t, point);
}

Eigen::Matrix2Xd
GlobFitCurve_Bezier::dposprojectdparams(const double t, const Eigen::VectorXd & dtprojectdparams)
{
  // sadly this won't work
  // return _bezier_impl->dposprojectdparams (  t, dtprojectdparams ) * _dbezierpts_dfittingparams;

  return dposdparams(t) + dposdt(t) * dtprojectdparams.transpose();
}

Eigen::Matrix2Xd
GlobFitCurve_Bezier::dposdtprojectdparams(const double t, const Eigen::VectorXd & dtprojectdparams)
{
  // sadly this won't work
  // return _bezier_impl->dposdtprojectdparams ( t, dtprojectdparams ) * _dbezierpts_dfittingparams;

  assert_break(_is_bezier_set);
  return dposdtdparams(t) + dposdtdt(t) * dtprojectdparams.transpose();
}

double
GlobFitCurve_Bezier::length()
{
  assert_break(_is_bezier_set);
  return _bezier_impl->length();
}

Eigen::VectorXd
GlobFitCurve_Bezier::dlengthdparams()
{
  // this one we are returning a col vector, so transpose everything
  assert_break(_is_bezier_set);
  return _dbezierpts_dfittingparams.transpose() * _bezier_impl->dlengthdparams();
}

void
GlobFitCurve_Bezier::set_params(const Eigen::VectorXd & params)
{
  assert_break(_is_bezier_set);
  _fitting_params = compute_fitting_params_from_vector(params);
  Eigen::MatrixXd control_pts = compute_bezier_control_points_from_struct(_fitting_params);
  get_bezier().set_control_points(control_pts);
  _dbezierpts_dfittingparams = compute_dbeziercontrolpts_dfittingparams(_fitting_params);
}

Eigen::VectorXd
GlobFitCurve_Bezier::get_params()
{
  assert_break(_is_bezier_set);
  return compute_fitting_params_from_struct(_fitting_params);
}

Eigen::Matrix2Xd
GlobFitCurve_Bezier::get_tesselation2()
{
  assert_break(_is_bezier_set);
  return get_bezier().get_tesselation2();
}


// Bezier interface
BezierCurve &
GlobFitCurve_Bezier::get_bezier()
{
  assert_break(_is_bezier_set);
  return *_bezier_impl;
}

void
GlobFitCurve_Bezier::set_bezier(BezierCurve & curve_in)
{
  _is_bezier_set = true;
  BezierCurve * bznew = new BezierCurve(curve_in);
  _bezier_impl.reset(bznew);
  _fitting_params = compute_fitting_params_from_control_points(_bezier_impl->get_control_points());
  _dbezierpts_dfittingparams = compute_dbeziercontrolpts_dfittingparams(_fitting_params);
}

Eigen::VectorXd
GlobFitCurve_Bezier::compute_fitting_params_from_struct(const FittingParams & in)
{
  Eigen::VectorXd ans(_n_params);
  ans << in.beg_tangent_len, in.end_tangent_len, in.beg_pt, in.end_pt, in.beg_tangent_angle, in.end_tangent_angle;
  return ans;
}

GlobFitCurve_Bezier::FittingParams
GlobFitCurve_Bezier::compute_fitting_params_from_vector(const Eigen::VectorXd & in)
{
  FittingParams ans;
  ans.beg_tangent_len = in(0);
  ans.end_tangent_len = in(1);
  ans.beg_pt = in.segment<2>(2);
  ans.end_pt = in.segment<2>(4);
  ans.beg_tangent_angle = in(6);
  ans.end_tangent_angle = in(7);
  return ans;
}

GlobFitCurve_Bezier::FittingParams
GlobFitCurve_Bezier::compute_fitting_params_from_control_points(const Eigen::Matrix2Xd & pts)
{

  FittingParams params;
  Eigen::Vector2d beg_tangent = pts.col(1) - pts.col(0);
  Eigen::Vector2d end_tangent = pts.col(3) - pts.col(2);

  auto get_angle = [](const Eigen::Vector2d in) -> double {
    const double len = in.squaredNorm();
    const double sn = in.y() / len;
    const double cs = in.x() / len;
    return std::atan2(sn, cs);
  };

  params.beg_pt = pts.col(0);
  params.end_pt = pts.col(3);
  params.beg_tangent_len = beg_tangent.norm();
  params.end_tangent_len = end_tangent.norm();
  params.beg_tangent_angle = get_angle(beg_tangent);
  params.end_tangent_angle = get_angle(end_tangent);

  return params;
}

Eigen::Matrix2Xd
GlobFitCurve_Bezier::compute_bezier_control_points_from_struct(const FittingParams & params)
{

  const double cs_beg = cos(params.beg_tangent_angle);
  const double sn_beg = sin(params.beg_tangent_angle);
  const double cs_end = cos(params.end_tangent_angle);
  const double sn_end = sin(params.end_tangent_angle);


  Eigen::Matrix2Xd ans(2, 4);

  ans.col(0) = params.beg_pt;
  ans.col(1) = params.beg_pt + params.beg_tangent_len * Eigen::Vector2d(cs_beg, sn_beg);
  ans.col(2) = params.end_pt - params.end_tangent_len * Eigen::Vector2d(cs_end, sn_end);
  ans.col(3) = params.end_pt;

  return ans;
}

Eigen::MatrixXd
GlobFitCurve_Bezier::compute_dbeziercontrolpts_dfittingparams(const FittingParams & in)
{

  const int n_bezier_points = 4;
  const int xid = 0;
  const int yid = 1;
  const int n_dim = 2;
  Eigen::MatrixXd ans(n_bezier_points * 2, _n_params);

  const double cs_beg = cos(in.beg_tangent_angle);
  const double sn_beg = sin(in.beg_tangent_angle);
  const double cs_end = cos(in.end_tangent_angle);
  const double sn_end = sin(in.end_tangent_angle);

  const double lbeg = in.beg_tangent_len;
  const double lend = in.end_tangent_len;


  ans.row(0 * n_dim + xid) << 0, 0, 1, 0, 0, 0, 0, 0;
  ans.row(0 * n_dim + yid) << 0, 0, 0, 1, 0, 0, 0, 0;
  //
  ans.row(1 * n_dim + xid) << cs_beg, 0, 1, 0, 0, 0, -lbeg * sn_beg, 0;
  ans.row(1 * n_dim + yid) << sn_beg, 0, 0, 1, 0, 0, lbeg * cs_beg, 0;
  //
  ans.row(2 * n_dim + xid) << 0, -cs_end, 0, 0, 1, 0, 0, lend * sn_end;
  ans.row(2 * n_dim + yid) << 0, -sn_end, 0, 0, 0, 1, 0, -lend * cs_end;
  //
  ans.row(3 * n_dim + xid) << 0, 0, 0, 0, 1, 0, 0, 0;
  ans.row(3 * n_dim + yid) << 0, 0, 0, 0, 0, 1, 0, 0;


  return ans;
}
