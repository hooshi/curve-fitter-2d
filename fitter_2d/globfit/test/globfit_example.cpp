#include <fitter_2d/bezier/bezier.hpp>
#include <fitter_2d/globfit/globfit_curve.hpp>
#include <fitter_2d/globfit/globfit_objective.hpp>
#include <fitter_2d/globfit/globfitter.hpp>
#include <fitter_2d/share/util.hpp>
#include <fitter_2d/share/vtk_curve_writer.hpp> // Optional

namespace
{

void
example_impl()
{

  //
  // Create input data.
  // Three bezier curves .
  // Some points for bezier 0 to fit distance.
  // Some points for bezier 1 to fit tangent.
  // Same pos transition between curves 0 and 1
  // Same tangent transition between curves 1 and 2
  //
  VtkCurveWriter writer;

  //
  // Bezier Curves and a line
  //
  GlobFitCurve_Bezier curve0, curve1;
  GlobFitCurve_Line curve2;

  // Curve 0
  {
    BezierCurve bzc0;
    Eigen::Matrix<double, 2, 4> control_points;
    control_points.row(0) << 0, 0.5, 0.4, 1;
    control_points.row(1) << 0, 0.3, 0.7, 1;
    bzc0.set_control_points(control_points);
    curve0.set_bezier(bzc0);

    for(int i = 0; i < 4; ++i)
    {
      writer.add_point(control_points.col(i));
    }

    writer.add_polyline(bzc0.get_tesselation2());
    writer.add_polyline(control_points);
  }

  // Curve 1
  {
    BezierCurve bzc1;
    Eigen::Matrix<double, 2, 4> control_points;
    control_points.row(0) << 1.1, 0.7, 0.8, 0;
    control_points.row(1) << 1.1, 1.1, 1.3, 2;
    bzc1.set_control_points(control_points);
    curve1.set_bezier(bzc1);

    for(int i = 0; i < 4; ++i)
    {
      writer.add_point(control_points.col(i));
    }

    writer.add_polyline(bzc1.get_tesselation2());
    writer.add_polyline(control_points);
  }

  // Curve 2
  {
    BezierCurve bzc2;
    Eigen::Matrix<double, 2, 2> end_points;
    end_points.row(0) << -0.1, 0.5; // x values
    end_points.row(1) << 2.1, 3; // y values
    curve2.set_points(end_points);

    for(int i = 0; i < 2; ++i)
    {
      writer.add_point(end_points.col(i));
    }

    writer.add_polyline(curve2.get_tesselation2());
  }


  //
  // Objectives
  //
  // cannot put the points in a block as the lambda will access out of
  // scope variables
  // Eigen::Vectorization is disabled, so it is fine to have vec<Eigen::Vec>
  GlobFitObjective_FitPoints obj_fit_points;
  GlobFitObjective_FitTangents obj_fit_tangents;
  GlobFitObjective_SamePosition obj_same_pos;
  GlobFitObjective_SameTangent obj_same_tangent;
  //
  GlobFitObjective_FixPosition obj_fix_pos_curve2;
  GlobFitObjective_FixPosition obj_fix_pos_curve0;
  GlobFitObjective_FixTangent obj_fix_tagent_curve0;
  GlobFitObjective_BezierFairness obj_bezier_devfrominit_curve1;
  GlobFitObjective_BezierFairness obj_bezier_devfrominit_curve0;

  //
  std::vector<Eigen::Vector2d> points_to_fit;
  std::vector<Eigen::Vector2d> tangents_to_fit_pos;
  std::vector<Eigen::Vector2d> tangents_to_fit_dir;
  std::vector<double> weights;
  points_to_fit = {Eigen::Vector2d(0.2, 0.2),
      Eigen::Vector2d(0.4, 0.6),
      Eigen::Vector2d(0.45, 0.55),
      Eigen::Vector2d(0.47, 0.36),
      Eigen::Vector2d(0.7, 0.5),
      Eigen::Vector2d(0.99, 0.58)};
  tangents_to_fit_pos = {Eigen::Vector2d(0.2, 0.8), Eigen::Vector2d(0.4, 0.9), Eigen::Vector2d(0.7, 1.3)};
  tangents_to_fit_dir = {Eigen::Vector2d(1, 1), Eigen::Vector2d(-1, 1), Eigen::Vector2d(0, 1)};
  weights = {1., 1., 1.};

  // Fit points objecitve -- curve0
  obj_fit_points.set_curve_ids(std::vector<int>({0 /*curve id*/}));
  obj_fit_points.set_points_to_fit(
      [&](int idx, Eigen::Vector2d & pt, double & w) {
        w = 1. / points_to_fit.size();
        pt = points_to_fit[idx];
      },
      (int)points_to_fit.size());
  obj_fit_points.set_weight(1.1);

  for(unsigned i = 0; i < points_to_fit.size(); ++i)
  {
    writer.add_point(points_to_fit.at(i));
  }

  // Fit tangents
  obj_fit_tangents.set_curve_ids(std::vector<int>({1 /*curve id*/}));
  obj_fit_tangents.set_points_to_fit(
      [&](int idx, Eigen::Vector2d & pt, Eigen::Vector2d & tang, double & w) {
        w = 1. / tangents_to_fit_pos.size();
        tang = tangents_to_fit_dir[idx];
        pt = tangents_to_fit_dir[idx];
      },
      (int)tangents_to_fit_pos.size());
  obj_fit_tangents.set_weight(1.1);

  // same pos
  obj_same_pos.set_curve_ids(std::vector<int>({0 /*curve 1*/, 1 /*curve 2*/}));
  obj_same_pos.set_t_for_same_position(1. /*curve 1 t*/, 0 /*curve 2 t*/);
  obj_same_pos.set_weight(100);

  // same tangent
  obj_same_tangent.set_curve_ids(std::vector<int>({1 /*curve 1*/, 2 /*curve 2*/}));
  obj_same_tangent.set_t_for_same_position(1. /*curve 1 t*/, 0 /*curve 2 t*/);
  obj_same_tangent.set_weight(100);

  // Fix stuff on curve 0
  obj_fix_pos_curve0.set_curve_ids(std::vector<int>({0}));
  obj_fix_pos_curve0.set_t_for_fixed_position(0); // beg
  obj_fix_pos_curve0.set_fixed_position(curve0.pos(0)); // remain constant
  obj_fix_pos_curve0.set_weight(100);
  //
  obj_fix_tagent_curve0.set_curve_ids(std::vector<int>({0}));
  obj_fix_tagent_curve0.set_t_for_fixed_tangent(0); // beg
  obj_fix_tagent_curve0.set_fixed_tangent(curve0.dposdt(0).normalized()); // remain constant
  obj_fix_tagent_curve0.set_weight(100);


  // Fix stuff on curve 2
  obj_fix_pos_curve2.set_curve_ids(std::vector<int>({2}));
  obj_fix_pos_curve2.set_t_for_fixed_position(1); // end
  obj_fix_pos_curve2.set_fixed_position(curve2.pos(1)); // remain constant
  obj_fix_pos_curve2.set_weight(100);

  // Don't let curve 0 move too much
  obj_bezier_devfrominit_curve0.set_initial_bezier(curve0.get_bezier().get_control_points());
  obj_bezier_devfrominit_curve0.set_weight(1);
  obj_bezier_devfrominit_curve0.set_sub_weights(10, 10, 1, 1., 2., 2.);
  obj_bezier_devfrominit_curve0.set_curve_ids(std::vector<int>({0}));

  // Don't let curve 1 move too much
  obj_bezier_devfrominit_curve1.set_initial_bezier(curve1.get_bezier().get_control_points());
  obj_bezier_devfrominit_curve1.set_weight(1);
  obj_bezier_devfrominit_curve1.set_sub_weights(10, 10, 1, 1., 2., 2.);
  obj_bezier_devfrominit_curve1.set_curve_ids(std::vector<int>({1}));


  writer.dump("test_dump/globfit_example_0.vtk");

  // Run the fitter
  auto callback = [&](int idx) {
    writer.clear();
    writer.add_polyline(curve0.get_tesselation2());
    writer.add_polyline(curve1.get_tesselation2());
    writer.add_polyline(curve2.get_tesselation2());
    writer.dump(STR("test_dump/globfit_example_" << idx + 1 << ".vtk"));
  };
  GlobFitter globfitter;
  globfitter.set_curves({&curve0, &curve1, &curve2});
  globfitter.set_objectives({&obj_fit_points,
      &obj_fit_tangents,
      &obj_same_tangent,
      &obj_same_pos,
      &obj_fix_pos_curve2,
      &obj_fix_pos_curve0,
      &obj_fix_tagent_curve0,
      &obj_bezier_devfrominit_curve0,
      &obj_bezier_devfrominit_curve1});
  globfitter.setup(20 /*max iter*/); // don't forget to call
  globfitter.run_fitter(true, callback);


} // end of function
} // end of anonymus

namespace deadlinecodetest
{
namespace globfitter
{
int
example(int /*argc*/, char ** /*argv*/)
{
  example_impl();
  return 0;
} // end of function
} // globfitter
} // deadline code test
