#include <fitter_2d/share/deriv_test.hpp>
#include <fitter_2d/share/util.hpp>
#include <fitter_2d/share/vtk_curve_writer.hpp>

#include <fitter_2d/bezier/bezier.hpp>
#include <fitter_2d/globfit/globfit_curve.hpp>
#include <fitter_2d/globfit/globfit_objective.hpp>
#include <fitter_2d/globfit/globfitter.hpp>

namespace
{

void
subtest_nonoverlapping_term()
{
}


// Must have called setup() on globfit before calling this
void
subtest_globfit_objective(GlobFitter & globfit, const std::string obj_name)
{

  assert_break(globfit.is_setup());
  printf("-- Testing Objective: %s \n", obj_name.c_str());

  Eigen::VectorXd params0 = globfit.get_params();
  Eigen::VectorXd delta = randn(globfit.n_parameters(), 1);
  double h0 = 1;
  int n_halving = 8;
  Eigen::VectorXd obj;
  Eigen::MatrixXd jac;


  // Test change of variables for fit
  derivtest::run(params0,
      delta,
      [&](const Eigen::VectorXd & new_params) -> Eigen::VectorXd {
        globfit.set_params(new_params);
        globfit.compute_objective_and_jacobian(obj, jac);
        return obj;
      },
      [&](const Eigen::VectorXd & hdelta) -> Eigen::VectorXd { //
        return jac * hdelta;
      },
      std::cout,
      n_halving,
      h0);
} // All done with subtest_globfit_objective
}

namespace deadlinecodetest
{
namespace globfitter
{
int
test_objectives(int /*argc*/, char ** /*argv*/)
{

  //
  // Create input data.
  // Three bezier curves .
  // Some points for bezier 0 to fit distance.
  // Some points for bezier 1 to fit tangent.
  // Same pos transition between curves 0 and 1
  // Same tangent transition between curves 1 and 2
  //
  VtkCurveWriter writer;


  //
  // Bezier Curves
  //
  GlobFitCurve_Bezier bz0, bz1, bz2;

  // Curve 0
  {
    BezierCurve bzc0;
    Eigen::Matrix<double, 2, 4> control_points;
    control_points.row(0) << 0, 0.5, 0.4, 1;
    control_points.row(1) << 0, 0.3, 0.7, 1;
    bzc0.set_control_points(control_points);
    bz0.set_bezier(bzc0);

    for(int i = 0; i < 4; ++i)
    {
      writer.add_point(control_points.col(i));
    }

    writer.add_polyline(bzc0.get_tesselation2());
    writer.add_polyline(control_points);
  }

  // Curve 1
  {
    BezierCurve bzc1;
    Eigen::Matrix<double, 2, 4> control_points;
    control_points.row(0) << 1.1, 0.7, 0.8, 0;
    control_points.row(1) << 1.1, 1.1, 1.3, 2;
    bzc1.set_control_points(control_points);
    bz1.set_bezier(bzc1);

    for(int i = 0; i < 4; ++i)
    {
      writer.add_point(control_points.col(i));
    }

    writer.add_polyline(bzc1.get_tesselation2());
    writer.add_polyline(control_points);
  }

  // Curve 2
  {
    BezierCurve bzc2;
    Eigen::Matrix<double, 2, 4> control_points;
    control_points.row(0) << -0.1, 0.5, -0.5, 0.5;
    control_points.row(1) << 2.1, 2.2, 2.5, 3;
    bzc2.set_control_points(control_points);
    bz2.set_bezier(bzc2);

    for(int i = 0; i < 4; ++i)
    {
      writer.add_point(control_points.col(i));
    }

    writer.add_polyline(bzc2.get_tesselation2());
    writer.add_polyline(control_points);
  }


  //
  // Objectives
  //
  // cannot put the points in a block as the lambda will access out of
  // scope variables
  // Eigen::Vectorization is disabled, so it is fine to have vec<Eigen::Vec>
  GlobFitObjective_FitPoints obj_fit_points;
  GlobFitObjective_FitTangents obj_fit_tangents;
  GlobFitObjective_SamePosition obj_same_pos;
  GlobFitObjective_SameTangent obj_same_tangent;
  GlobFitObjective_FixPosition obj_fix_pos;
  GlobFitObjective_FixTangent obj_fix_tangent;
  GlobFitObjective_BezierFairness obj_bezier_devfrominit;

  //
  std::vector<Eigen::Vector2d> points_to_fit;
  std::vector<Eigen::Vector2d> tangents_to_fit_pos;
  std::vector<Eigen::Vector2d> tangents_to_fit_dir;
  std::vector<double> weights;
  points_to_fit = {Eigen::Vector2d(0.2, 0.2), Eigen::Vector2d(0.4, 0.6), Eigen::Vector2d(0.7, 0.5)};
  tangents_to_fit_pos = {Eigen::Vector2d(0.2, 0.8), Eigen::Vector2d(0.4, 0.9), Eigen::Vector2d(0.7, 1.3)};
  tangents_to_fit_dir = {Eigen::Vector2d(1, 1), Eigen::Vector2d(-1, 1), Eigen::Vector2d(0, 1)};
  weights = {1., 1., 1.};

  // Fit points objecitve
  obj_fit_points.set_curve_ids(std::vector<int>({0}));
  obj_fit_points.set_points_to_fit(
      [&](int idx, Eigen::Vector2d & pt, double & w) {
        w = weights[idx];
        pt = points_to_fit[idx];
      },
      (int)points_to_fit.size());
  obj_fit_points.set_weight(1.1);

  for(unsigned i = 0; i < points_to_fit.size(); ++i)
  {
    writer.add_point(points_to_fit.at(i));
  }

  // Fit tangents
  obj_fit_tangents.set_curve_ids(std::vector<int>({1}));
  obj_fit_tangents.set_points_to_fit(
      [&](int idx, Eigen::Vector2d & pt, Eigen::Vector2d & tang, double & w) {
        w = weights[idx];
        tang = tangents_to_fit_dir[idx];
        pt = tangents_to_fit_dir[idx];
      },
      (int)points_to_fit.size());
  obj_fit_tangents.set_weight(1.1);

  // same pos
  obj_same_pos.set_curve_ids(std::vector<int>({0, 1}));
  obj_same_pos.set_t_for_same_position(1., 0);
  obj_same_pos.set_weight(1.2);

  // same tangent
  obj_same_tangent.set_curve_ids(std::vector<int>({1, 2}));
  obj_same_tangent.set_t_for_same_position(1., 0);
  obj_same_tangent.set_weight(0.9);


  // Fix pos
  obj_fix_pos.set_curve_ids(std::vector<int>({0}));
  obj_fix_pos.set_t_for_fixed_position(0.);
  obj_fix_pos.set_weight(0.85);
  obj_fix_pos.set_fixed_position(randn(2, 1));


  // Fix tangent
  obj_fix_tangent.set_curve_ids(std::vector<int>({2}));
  obj_fix_tangent.set_t_for_fixed_tangent(0.6568);
  obj_fix_tangent.set_weight(1.3);
  obj_fix_tangent.set_fixed_tangent(randn(2, 1).normalized());

  // bezier dev from init
  obj_bezier_devfrominit.set_curve_ids(std::vector<int>({1}));
  obj_bezier_devfrominit.set_weight(1);
  obj_bezier_devfrominit.set_sub_weights(1.1, 0.86, 0.3565, .12312, 2., 1.5);
  obj_bezier_devfrominit.set_initial_bezier(bz0.get_bezier().get_control_points());

  //
  // Test fitting points
  //
  {
    GlobFitter globfitter;
    globfitter.set_curves({&bz0, &bz1, &bz2});
    globfitter.set_objectives({&obj_fit_points});
    globfitter.setup(60 /* maxiter -- does not matter in this case */);
    globfitter.set_params(globfitter.get_params());
    subtest_globfit_objective(globfitter, "POSITION FITTER OBJECTIVE");
  }

  //
  // Test fitting tangents
  //
  {
    GlobFitter globfitter;
    globfitter.set_curves({&bz0, &bz1, &bz2});
    globfitter.set_objectives({&obj_fit_tangents});
    globfitter.setup(60 /* maxiter -- does not matter in this case */);
    globfitter.set_params(globfitter.get_params());
    subtest_globfit_objective(globfitter, "TANGENT FITTER OBJECTIVE");
  }

  //
  // Test same pos
  //
  {
    GlobFitter globfitter;
    globfitter.set_curves({&bz0, &bz1, &bz2});
    globfitter.set_objectives({&obj_same_pos});
    globfitter.setup(60 /* maxiter -- does not matter in this case */);
    globfitter.set_params(globfitter.get_params());
    subtest_globfit_objective(globfitter, "SAMEPOS FITTER OBJECTIVE");
  }

  //
  // Test same tangent
  //
  {
    GlobFitter globfitter;
    globfitter.set_curves({&bz0, &bz1, &bz2});
    globfitter.set_objectives({&obj_same_tangent});
    globfitter.setup(60 /* maxiter -- does not matter in this case */);
    globfitter.set_params(globfitter.get_params());
    subtest_globfit_objective(globfitter, "SAME TANGENT FITTER OBJECTIVE");
  }

  //
  // Test fix pos
  //
  {
    GlobFitter globfitter;
    globfitter.set_curves({&bz0, &bz1, &bz2});
    globfitter.set_objectives({&obj_fix_pos});
    globfitter.setup(60 /* maxiter -- does not matter in this case */);
    globfitter.set_params(globfitter.get_params());
    subtest_globfit_objective(globfitter, "FIX POS OBJECTIVE");
  }

  //
  // Test fix tangent
  //
  {
    GlobFitter globfitter;
    globfitter.set_curves({&bz0, &bz1, &bz2});
    globfitter.set_objectives({&obj_fix_tangent});
    globfitter.setup(60 /* maxiter -- does not matter in this case */);
    globfitter.set_params(globfitter.get_params());
    subtest_globfit_objective(globfitter, "FIX TANGENT  OBJECTIVE");
  }

  //
  // Test the bezier dev from init
  //
  {
    GlobFitter globfitter;
    globfitter.set_curves({&bz0, &bz1, &bz2});
    globfitter.set_objectives({&obj_bezier_devfrominit});
    globfitter.setup(60 /* maxiter -- does not matter in this case */);
    globfitter.set_params(globfitter.get_params());
    subtest_globfit_objective(globfitter, "BEZIER DEV FROM INIT OBJECTIVE");
  }

  //
  // Now all together
  //
  {
    GlobFitter globfitter;
    globfitter.set_curves({&bz0, &bz1, &bz2});
    globfitter.set_objectives({&obj_fit_points,
        &obj_fit_tangents,
        &obj_same_tangent,
        &obj_same_pos,
        &obj_fix_pos,
        &obj_fix_tangent,
        &obj_bezier_devfrominit});
    globfitter.setup(60 /* maxiter -- does not matter in this case */);
    globfitter.set_params(globfitter.get_params());
    subtest_globfit_objective(globfitter, "ALL TOGETHER");
  }

  writer.dump("test_dump/test_objectives.vtk");

  return 0;
}
} // globfitter
} // deadline code test