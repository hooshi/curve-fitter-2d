#ifndef DEADLINE_CODE_GLOBFIT_OBJECTIVE_IS_INCLUDED
#define DEADLINE_CODE_GLOBFIT_OBJECTIVE_IS_INCLUDED

#include <vector>

#include <Eigen/Core>

#include <fitter_2d/globfit/globfit_curve.hpp>
#include <fitter_2d/share/util.hpp>

enum GlobFitObjectiveType
{
  // A curve tries to fit a series of points as good as possible
  GLOBFIT_OBJECTIVE_FIT_POINTS = 0,
  // A curve tries to fit  tangents at a series of points as good as possible
  GLOBFIT_OBJECTIVE_FIT_TANGENTS = 1,
  // A curve tries to have a certain position at a certain t value
  GLOBFIT_OBJECTIVE_FIX_POSITION = 2,
  // A curve tries to have a certain tangent at a certain t value
  GLOBFIT_OBJECTIVE_FIX_TANGENT = 3,
  // two curves try to attain the same position at certain t values
  GLOBFIT_OBJECTIVE_SAME_POSITION = 4,
  // two curves try to attain the same tangents at certain t values
  GLOBFIT_OBJECTIVE_SAME_TANGENT = 5,
  // Objecitves to control bezier fairness
  GLOBFIT_OBJECTIVE_BEZIER_FAIRNESS = 6,
};

// =============================================================
//                           BASE CLASS
// =============================================================
class GlobFitCurve;

class GlobFitObjective
{
public:
  template<typename VectorType>
  void set_curve_ids(VectorType curve_ids);
  const std::vector<int> & get_curve_ids() const;

  void set_weight(const double);
  double get_weight();
  double get_sqrt_weight();

  virtual int n_equations() = 0;
  virtual GlobFitObjectiveType get_type() = 0;

  // always call this before calling compute_objective_and_jacobian()
  void set_curves(std::vector<GlobFitCurve *>);
  virtual void compute_objective_and_jacobian(Eigen::VectorXd & obj, Eigen::MatrixXd & dobj_dcurveparams) = 0;

  GlobFitObjective() = default;
  virtual ~GlobFitObjective() = default;

protected:
  std::vector<int> _curve_ids = std::vector<int>();
  double _weight = -1000;
  double _sqrt_weight = -1000;

  // does not own them
  std::vector<GlobFitCurve *> _cached_curves = std::vector<GlobFitCurve *>();

  void _forget_curves();
  int _n_sum_curve_params();
  virtual bool _is_all_data_provided();
};

template<typename VectorType>
void
GlobFitObjective::set_curve_ids(VectorType curve_ids)
{
  _curve_ids.resize(curve_ids.size());

  for(unsigned i = 0; i < curve_ids.size(); ++i)
  {
    _curve_ids[i] = (int)curve_ids[i];
  }
}

// =============================================================
//                        Fit Points
// =============================================================

class GlobFitObjective_FitPoints : public GlobFitObjective
{
public:
  using PointWeightFeeder = std::function<void(int, Eigen::Vector2d &, double &)>;

  void set_points_to_fit(PointWeightFeeder, const int n_points);
  void get_points_to_fit(PointWeightFeeder & feeder, int & n_points) const;

  int n_equations();
  GlobFitObjectiveType get_type();
  void compute_objective_and_jacobian(Eigen::VectorXd & obj, Eigen::MatrixXd & dobj_dcurveparams);

  GlobFitObjective_FitPoints() = default;

private:
  PointWeightFeeder _point_feeder = PointWeightFeeder(nullptr);
  int _n_points_to_fit = 0;

  bool _is_all_data_provided();
};

// =============================================================
//                        FIT TANGENTS
// =============================================================

class GlobFitObjective_FitTangents : public GlobFitObjective
{
public:
  using PointTangentWeightFeeder = std::function<void(int, Eigen::Vector2d &, Eigen::Vector2d &, double &)>;

  void set_points_to_fit(PointTangentWeightFeeder, const int n_points);
  void get_points_to_fit(PointTangentWeightFeeder & feeder, int & n_points) const;

  int n_equations();
  GlobFitObjectiveType get_type();
  void compute_objective_and_jacobian(Eigen::VectorXd & obj, Eigen::MatrixXd & dobj_dcurveparams);

  GlobFitObjective_FitTangents() = default;

private:
  PointTangentWeightFeeder _tangent_feeder = PointTangentWeightFeeder(nullptr);
  int _n_points_to_fit = 0;

  bool _is_all_data_provided();
};

// =============================================================
//                          FIX POSITION
// =============================================================

class GlobFitObjective_FixPosition : public GlobFitObjective
{
public:
  void set_t_for_fixed_position(const double t_curve);
  void set_fixed_position(const Eigen::Vector2d &);

  int n_equations();
  GlobFitObjectiveType get_type();
  void compute_objective_and_jacobian(Eigen::VectorXd & obj, Eigen::MatrixXd & dobj_dcurveparams);

private:
  double _t_curve = -1;
  Eigen::Vector2d _fixed_pos = Eigen::Vector2d::Constant(std::numeric_limits<double>::max());

  bool _is_all_data_provided();
};

// =============================================================
//                          FIX TANGENT
// =============================================================

class GlobFitObjective_FixTangent : public GlobFitObjective
{
public:
  void set_t_for_fixed_tangent(const double t_curve);
  void set_fixed_tangent(const Eigen::Vector2d &);

  int n_equations();
  GlobFitObjectiveType get_type();
  void compute_objective_and_jacobian(Eigen::VectorXd & obj, Eigen::MatrixXd & dobj_dcurveparams);

private:
  double _t_curve = -1;
  Eigen::Vector2d _fixed_tangent = Eigen::Vector2d::Constant(std::numeric_limits<double>::max());

  bool _is_all_data_provided();
};

// =============================================================
//                          SAME POSITION
// =============================================================

class GlobFitObjective_SamePosition : public GlobFitObjective
{
public:
  void set_t_for_same_position(const double t_curve0, const double t_curve1);
  void get_t_for_same_position(double & t_curve0, double & t_curve1);

  int n_equations();
  GlobFitObjectiveType get_type();
  void compute_objective_and_jacobian(Eigen::VectorXd & obj, Eigen::MatrixXd & dobj_dcurveparams);

private:
  double _t_curve0 = -1;
  double _t_curve1 = -1;

  bool _is_all_data_provided();
};

// =============================================================
//                          SAME TANGENT
// =============================================================

class GlobFitObjective_SameTangent : public GlobFitObjective
{
public:
  void set_t_for_same_position(const double t_curve0, const double t_curve1);
  void get_t_for_same_position(double & t_curve0, double & t_curve1);

  int n_equations();
  GlobFitObjectiveType get_type();
  void compute_objective_and_jacobian(Eigen::VectorXd & obj, Eigen::MatrixXd & dobj_dcurveparams);

private:
  double _t_curve0 = -1;
  double _t_curve1 = -1;

  bool _is_all_data_provided();
};

// =============================================================
//                BEZIER SMALL DEVIATION FROM INIT
// =============================================================

class GlobFitObjective_BezierFairness : public GlobFitObjective
{
public:
  // This will make an internal copy, must be called
  void set_initial_bezier(const Eigen::Matrix2Xd & control_points);
  void set_sub_weights(const double weight_end_tangent_length_dev,
      const double weight_end_tangent_angle_dev,
      const double weight_end_points_dev,
      const double weight_length,
      const double weight_prevent_control_point_overlap,
      const double thr_prevent_control_point_overlap);

  int n_equations();
  GlobFitObjectiveType get_type();
  void compute_objective_and_jacobian(Eigen::VectorXd & obj, Eigen::MatrixXd & dobj_dcurveparams);

private:
  bool _is_initial_bezier_set;
  double _weight_end_tangent_length_dev = -1;
  double _weight_end_tangent_angle_dev = -1;
  double _weight_end_points_dev = -1;
  double _weight_length = -1;
  double _weight_prevent_control_point_overlap = -1.;
  double _thr_prevent_control_point_overlap = 1.;
  GlobFitCurve_Bezier _initial_bezier;
  bool _is_all_data_provided();
};

#endif // DEADLINE_CODE_GLOBFIT_OBJECTIVE_IS_INCLUDED
