#ifndef DEADLINE_CODE_GLOBFIT_CURVE_IS_INCLUDED
#define DEADLINE_CODE_GLOBFIT_CURVE_IS_INCLUDED

#include <memory>

#include <Eigen/Core>

#include <fitter_2d/share/util.hpp>


enum GlobFitCurveType
{
  GLOBFIT_CURVE_LINE = 0,
  GLOBFIT_CURVE_ARC = 1,
  GLOBFIT_CURVE_BEZIER = 2,
};

// =============================================================
//                           BASE CLASS
// =============================================================

//
// This is the curve interface used by the globfitter.
// Each curve that is used by the glob fitter
// has to be wrapped inside one of these objects.
//

class GlobFitCurve
{
public:
  static constexpr double t_end = 1.;

  virtual GlobFitCurveType get_type() = 0;
  virtual int n_params() = 0;

  virtual Eigen::Vector2d pos(const double t) = 0;
  virtual Eigen::Vector2d dposdt(const double t) = 0;
  virtual Eigen::Vector2d dposdtdt(const double t) = 0;

  virtual Eigen::Matrix2Xd dposdparams(const double t) = 0;
  virtual Eigen::Matrix2Xd dposdtdparams(const double t) = 0;

  virtual double project(const Eigen::Vector2d & point) = 0;

  // This has a default implementation
  virtual Eigen::VectorXd dtprojectdparams(const double t, const Eigen::Vector2d & point) = 0;

  virtual Eigen::Matrix2Xd dposprojectdparams(const double t, const Eigen::VectorXd & dtprojectdparams) = 0;
  virtual Eigen::Matrix2Xd dposdtprojectdparams(const double t, const Eigen::VectorXd & dtprojectdparams) = 0;


  virtual void set_params(const Eigen::VectorXd & params) = 0;
  virtual Eigen::VectorXd get_params() = 0;

  virtual Eigen::Matrix2Xd get_tesselation2() = 0;

  virtual ~GlobFitCurve() {}
};

// =============================================================
//                              LINE
// =============================================================

// Rather than wrapping curnucopia's line, it seems to be easier to
// just rewrite this one.
// The better practice .
class GlobFitCurve_Line : public GlobFitCurve
{
public:
  GlobFitCurveType get_type() override;
  int n_params() override;

  Eigen::Vector2d pos(const double t) override;
  Eigen::Vector2d dposdt(const double t) override;
  Eigen::Vector2d dposdtdt(const double t) override;

  Eigen::Matrix2Xd dposdparams(const double t) override;
  Eigen::Matrix2Xd dposdtdparams(const double t) override;

  double project(const Eigen::Vector2d & point) override;

  Eigen::VectorXd dtprojectdparams(const double t, const Eigen::Vector2d & point) override;
  Eigen::Matrix2Xd dposprojectdparams(const double t, const Eigen::VectorXd & dtprojectdparams) override;
  Eigen::Matrix2Xd dposdtprojectdparams(const double t, const Eigen::VectorXd & dtprojectdparams) override;


  void set_params(const Eigen::VectorXd & params) override;
  Eigen::VectorXd get_params() override;

  Eigen::Matrix2Xd get_tesselation2() override;

  // Line functions
  void set_points(const Eigen::Matrix2d & points);
  Eigen::Matrix2d get_points();

  GlobFitCurve_Line() = default;
  ~GlobFitCurve_Line() {}

private:
  static constexpr int _n_params = 4;
  bool _are_points_set = false;
  // First column is first point, second point is the second point
  Eigen::Matrix2d _points = Eigen::Matrix2d::Constant(1e10);
};

// =============================================================
//                               ARC
// =============================================================

//
// This one wraps the cornucopia object.
// Ideally , we should kill the cornucopia arc, write our own arc,
// and finally wrap our own arc here.
// If we are crazy, we can even one  class wrapping a new arc,
// and once class wrapping the Cornucopia's arc. Everything is possible.
//
struct Arc;

class GlobFitCurve_Arc : public GlobFitCurve
{

public:
  GlobFitCurveType get_type() override;
  int n_params() override;

  Eigen::Vector2d pos(const double t) override;
  Eigen::Vector2d dposdt(const double t) override;
  Eigen::Vector2d dposdtdt(const double t) override;

  Eigen::Matrix2Xd dposdparams(const double t) override;
  Eigen::Matrix2Xd dposdtdparams(const double t) override;

  double project(const Eigen::Vector2d & point) override;

  Eigen::VectorXd dtprojectdparams(const double t, const Eigen::Vector2d & point) override;
  Eigen::Matrix2Xd dposprojectdparams(const double t, const Eigen::VectorXd & dtprojectdparams) override;
  Eigen::Matrix2Xd dposdtprojectdparams(const double t, const Eigen::VectorXd & dtprojectdparams) override;


  void set_params(const Eigen::VectorXd & params) override;
  Eigen::VectorXd get_params() override;

  Eigen::Matrix2Xd get_tesselation2() override;

  // Arc -- makes copies
  void set_cornu_arc(Arc &);
  Arc & get_cornu_arc();

  GlobFitCurve_Arc() = default;
  ~GlobFitCurve_Arc() = default;

private:
  constexpr static int _n_params = 5;
  bool _is_arc_set = false;
  std::unique_ptr<Arc> _arc_impl; // allows partial definition
};

// =============================================================
//                             BEZIER
// =============================================================

// Params are
// 1- Distance of second point minus first one 1
// 2- Distance of third point minus fourth one 1
// 3- Position of first point 2
// 4- Position of last point  2
// 5- First angle 1
// 6- Last angle  1
//
// This class has a lot of overlapping functionality with BezierFitter
// Would be nice to merge them, but not now.
// A nice way to do this is changing bezier fitter to bezier fit into or something.
//
// NOTE: makes a copy of an internal bezier curve.
// NOTE: wraps bezier/bezier.hpp
// NOTE: rather than control points solves for the mentioned parameters.
//       therefore, a change of parameters is made.
//
class BezierCurve;

class GlobFitCurve_Bezier : public GlobFitCurve
{
public:
  // GlobFitCurve Curve interface
  GlobFitCurveType get_type() override;
  int n_params() override;

  Eigen::Vector2d pos(const double t) override;
  Eigen::Vector2d dposdt(const double t) override;
  Eigen::Vector2d dposdtdt(const double t) override;

  Eigen::Matrix2Xd dposdparams(const double t) override;
  Eigen::Matrix2Xd dposdtdparams(const double t) override;

  double project(const Eigen::Vector2d & point) override;

  Eigen::VectorXd dtprojectdparams(const double t, const Eigen::Vector2d & point) override;
  Eigen::Matrix2Xd dposprojectdparams(const double t, const Eigen::VectorXd & dtprojectdparams) override;
  Eigen::Matrix2Xd dposdtprojectdparams(const double t, const Eigen::VectorXd & dtprojectdparams) override;

  void set_params(const Eigen::VectorXd & params) override;
  Eigen::VectorXd get_params() override;

  Eigen::Matrix2Xd get_tesselation2() override;


  // Bezier interface
  double length();
  Eigen::VectorXd dlengthdparams();

  BezierCurve & get_bezier();
  void set_bezier(BezierCurve &);

  GlobFitCurve_Bezier() = default;
  ~GlobFitCurve_Bezier() = default;

private:
  static constexpr int _n_params = 8;
  bool _is_bezier_set = false;

  struct FittingParams
  {
    double beg_tangent_len;
    double end_tangent_len;
    Eigen::Vector2d beg_pt;
    Eigen::Vector2d end_pt;
    double beg_tangent_angle;
    double end_tangent_angle;
  };

  static Eigen::VectorXd compute_fitting_params_from_struct(const FittingParams &);
  static FittingParams compute_fitting_params_from_vector(const Eigen::VectorXd &);
  static FittingParams compute_fitting_params_from_control_points(const Eigen::Matrix2Xd &);
  static Eigen::Matrix2Xd compute_bezier_control_points_from_struct(const FittingParams &);

  static Eigen::MatrixXd compute_dbeziercontrolpts_dfittingparams(const FittingParams &);

  std::unique_ptr<BezierCurve> _bezier_impl; // allows partial definition
  FittingParams _fitting_params;
  Eigen::MatrixXd _dbezierpts_dfittingparams;
};


#endif // DEADLINE_CODE_GLOBFIT_CURVE_IS_INCLUDED
