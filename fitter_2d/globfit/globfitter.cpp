#include <fitter_2d/globfit/globfit_curve.hpp>
#include <fitter_2d/globfit/globfit_objective.hpp>
#include <fitter_2d/globfit/globfitter.hpp>

void
GlobFitter::set_curves(const std::vector<GlobFitCurve *> & curves_in)
{
  _curves = curves_in;
  _is_setup = false;
}

void
GlobFitter::set_objectives(const std::vector<GlobFitObjective *> & objectives_in)
{
  _objectives = objectives_in;
  _is_setup = false;
}

void
GlobFitter::setup(const int max_iterations)
{
  _max_iterations = max_iterations;
  assert_break(_objectives.size());
  assert_break(_curves.size());

  //
  // Equation bookkeeping
  //
  _xadj_equations.resize(_objectives.size() + 1);
  _xadj_equations[0] = 0;

  for(int i = 0; i < (int)_objectives.size(); ++i)
  {
    _xadj_equations[i + 1] = _xadj_equations[i] + _objectives[i]->n_equations();
  }

  //
  // Parameter bookkeeping
  //
  _xadj_parameters.resize(_curves.size() + 1);
  _xadj_parameters[0] = 0;

  for(int i = 0; i < (int)_curves.size(); ++i)
  {
    _xadj_parameters[i + 1] = _xadj_parameters[i] + _curves[i]->n_params();
  }

  //
  // Initial params
  //

  _is_setup = true;
}

int
GlobFitter::n_parameters()
{
  assert_break(_is_setup);
  return _xadj_parameters.back();
}

int
GlobFitter::n_equations()
{
  assert_break(_is_setup);
  return _xadj_equations.back();
}

bool
GlobFitter::is_setup()
{
  return _is_setup;
}


std::vector<GlobFitCurve *>
GlobFitter::get_curves()
{ //
  return _curves;
}


std::vector<GlobFitObjective *>
GlobFitter::get_objectives()
{ //
  return _objectives;
}



void
GlobFitter::compute_objective_and_jacobian(Eigen::VectorXd & obj, Eigen::MatrixXd & jac)
{ //
  assert_break(_is_setup);

  obj.setZero(n_equations());
  jac.setZero(n_equations(), n_parameters());
  std::vector<GlobFitCurve *> subcurves;
  Eigen::VectorXd sub_obj;
  Eigen::MatrixXd sub_jac;

  for(int objid = 0; objid < (int)_objectives.size(); ++objid)
  {
    const int eq_beg = _xadj_equations[objid];
    const int eq_len = _xadj_equations[objid + 1] - _xadj_equations[objid];

    const int n_subcurves = (int)_objectives[objid]->get_curve_ids().size();

    // Set the curves for the objective object
    subcurves.resize(n_subcurves);

    for(int curveoffset = 0; curveoffset < n_subcurves; ++curveoffset)
    {
      const int curveid = _objectives[objid]->get_curve_ids()[curveoffset];
      subcurves[curveoffset] = _curves[curveid];
    }

    _objectives[objid]->set_curves(subcurves);

    // Eval objective
    _objectives[objid]->compute_objective_and_jacobian(sub_obj, sub_jac);

    // Set obj
    obj.segment(eq_beg, eq_len) = _objectives[objid]->get_sqrt_weight() * sub_obj;


    // Set Jacobian
    int subjac_colid = 0;

    for(int curveoffset = 0; curveoffset < n_subcurves; ++curveoffset)
    {
      const int curveid = _objectives[objid]->get_curve_ids()[curveoffset];

      const int param_beg = _xadj_parameters[curveid];
      const int param_len = _xadj_parameters[curveid + 1] - _xadj_parameters[curveid];

      jac.block(eq_beg, param_beg, eq_len, param_len) =
          _objectives[objid]->get_sqrt_weight() * sub_jac.middleCols(subjac_colid, param_len);
      subjac_colid += param_len;
    } // end of curves
  } // end of objectives
}

Eigen::VectorXd
GlobFitter::get_params()
{ //
  assert_break(_is_setup);
  Eigen::VectorXd ans(n_parameters());

  for(int i = 0; i < (int)_curves.size(); ++i)
  {
    const int beg = _xadj_parameters[i];
    const int len = _xadj_parameters[i + 1] - _xadj_parameters[i];
    ans.segment(beg, len) = _curves[i]->get_params();
  }

  return ans;
}

void
GlobFitter::set_params(const Eigen::VectorXd & params_in)
{ //
  for(int i = 0; i < (int)_curves.size(); ++i)
  {
    const int beg = _xadj_parameters[i];
    const int len = _xadj_parameters[i + 1] - _xadj_parameters[i];
    _curves[i]->set_params(params_in.segment(beg, len));
  }
}
