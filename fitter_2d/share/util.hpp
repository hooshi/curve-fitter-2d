#ifndef DEADLINE_CODE_BEZIER_UTIL_IS_INCLUDED
#define DEADLINE_CODE_BEZIER_UTIL_IS_INCLUDED

#include <Eigen/Core>
#include <sstream>

#ifndef STR
#define STR(X) static_cast<std::ostringstream &>(std::ostringstream().flush() << X).str()
#endif

#ifndef C_STR
#define C_STR(X) STR(X).c_str()
#endif

#ifndef assert_break
#define assert_break_msg(X, msg)                                         \
  if(!(X))                                                               \
  {                                                                      \
    printf("Assertion error at file:%s line:%d \n", __FILE__, __LINE__); \
    printf("Content : %s \n", #X);                                       \
    printf("Msg     : %s \n", C_STR(msg));                               \
    fflush(stdout);                                                      \
    throw std::exception();                                              \
  }
#define assert_break(X) assert_break_msg(X, "")
#endif

#ifndef param_unused
#define param_unused(X) (void)(X)
#endif


Eigen::MatrixXd
normal_dist(const Eigen::MatrixXd & in);

Eigen::MatrixXd
randn(unsigned int i, unsigned int j);

Eigen::MatrixXd
reshaped(const Eigen::MatrixXd & in, int m, int n);

#endif // DEADLINE_CODE_BEZIER_UTIL_IS_INCLUDED