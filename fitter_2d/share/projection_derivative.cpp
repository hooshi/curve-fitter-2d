#include <fitter_2d/share/projection_derivative.hpp>

namespace ProjectionDerivative
{

Eigen::VectorXd
eval(const int n_params,
    const double time,
    const double tend,
    const Eigen::Vector2d & outside_point,
    const Eigen::Vector2d & rr,
    const Eigen::Vector2d & drdt,
    const Eigen::Vector2d & drdtdt,
    const Eigen::Matrix2Xd & drdparams,
    const Eigen::Matrix2Xd & drdtdparams)
{

  const double tol = 1e-10;

  //
  // find z.drdt
  //
  const Eigen::Vector2d zz = rr - outside_point;
  const double zdotdrdt = zz.dot(drdt);
  const bool is_bdry_minimizer = std::abs(zdotdrdt) > tol;

  //
  // Find the dt/dparamstilde
  //
  Eigen::MatrixXd dtdparamstilde(1, n_params);

  if(is_bdry_minimizer && ((time <= tol) || (time + tol >= tend)))
  {
    dtdparamstilde.setZero(); // Assuming dtend and dtbeg / dparams = 0
  }
  else
  {
    double denom = drdt.dot(drdt) + zz.dot(drdtdt);

    if(std::abs(denom) < tol)
    {
      denom = (denom < 0. ? -tol : tol);
    }

    dtdparamstilde = -(drdt.transpose() * drdparams + zz.transpose() * drdtdparams) / denom;
  }


  // theoreticlly it is a rowvector, but all single row should become col
  return dtdparamstilde.transpose();
}

} // end of ProjectionDerivative
