#include <fitter_2d/share/util.hpp>


Eigen::MatrixXd
normal_dist(const Eigen::MatrixXd & in)
{
  Eigen::MatrixXd out(in.rows(), in.cols());
  const double sqrtpiinv = 1. / std::sqrt(2 * 3.14); // don't use for places where need accurate answer

  for(unsigned i = 0; i < in.rows(); ++i)
  {
    for(unsigned j = 0; j < in.cols(); ++j)
    {
      out(i, j) = sqrtpiinv * std::exp(-in(i, j) * in(i, j) / 2.);
    }
  }

  return out;
}

Eigen::MatrixXd
randn(unsigned int i, unsigned int j)
{
  return normal_dist(Eigen::MatrixXd::Random(i, j));
}

Eigen::MatrixXd
reshaped(const Eigen::MatrixXd & in, int m, int n)
{
  assert(in.size() == m * n);
  return Eigen::Map<const Eigen::MatrixXd>(in.data(), m, n);
}
