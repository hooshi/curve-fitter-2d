#ifndef PROJECTION_DERIVATIVE
#define PROJECTION_DERIVATIVE

#include <Eigen/Core>

namespace ProjectionDerivative
{

Eigen::VectorXd
eval(const int n_params,
    const double time,
    const double tend,
    const Eigen::Vector2d & outside_point,
    const Eigen::Vector2d & rr,
    const Eigen::Vector2d & drdt,
    const Eigen::Vector2d & drdtdt,
    const Eigen::Matrix2Xd & drdparams,
    const Eigen::Matrix2Xd & drdtdparams);
}



#endif // LINE_LINE_INTERSECTION
