#ifndef LINE_LINE_INTERSECTION
#define LINE_LINE_INTERSECTION

#include <Eigen/Core>

namespace LineLineIntresection
{

Eigen::Vector2d
eval_pos(const Eigen::Vector2d & line0_begin, const Eigen::Vector2d & line0_end, const double line_param);

void
eval_intersection(const Eigen::Vector2d & line0_begin,
    const Eigen::Vector2d & line0_end,
    const Eigen::Vector2d & line1_begin,
    const Eigen::Vector2d & line1_end,
    bool & do_intersect,
    double & intersection_param_on_line_1);
}



#endif // LINE_LINE_INTERSECTION