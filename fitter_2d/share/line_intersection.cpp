#include <fitter_2d/share/line_intersection.hpp>

Eigen::Vector2d
LineLineIntresection::eval_pos(const Eigen::Vector2d & line0_begin,
    const Eigen::Vector2d & line0_end,
    const double line_param)
{
  return (line0_end - line0_begin) * line_param + line0_begin;
}

void
LineLineIntresection::eval_intersection(const Eigen::Vector2d & line0_begin,
    const Eigen::Vector2d & line0_end,
    const Eigen::Vector2d & line1_begin,
    const Eigen::Vector2d & line1_end,
    bool & do_intersect,
    double & intersection_param_on_line_1)
{

  const double tol = 1e-15;

  const Eigen::Vector2d tang_line0 = (line0_end - line1_begin);
  const Eigen::Vector2d tang_norm_line0 = (tang_line0).normalized();
  const Eigen::Vector2d n_line0(-tang_line0.y(), tang_line0.x());

  const Eigen::Vector2d tang_line1 = (line1_end - line1_begin);

  const double dot_prod = tang_line1.dot(n_line0);

  if(std::abs(dot_prod) > tol)
  {
    do_intersect = false;
  }
  else
  {
    do_intersect = true;
    intersection_param_on_line_1 = line0_begin.dot(n_line0) / dot_prod;
  }
}
