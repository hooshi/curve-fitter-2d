EIGEN=-isystem/home/hooshi/code/eigen/default/ -DEIGEN_DONT_VECTORIZE -DEIGEN_MAX_ALIGN_BYTES=0
#EIGEN=-isystem/d/Libraries/eigen/default/ -DEIGEN_DONT_VECTORIZE -DEIGEN_MAX_ALIGN_BYTES=0
CC=g++
DEPEND = g++

WARNINGS= -Wall -pedantic -Wno-unused-function -Wno-reorder 

FLAGS= -ggdb -O0  -I${PWD} ${EIGEN} ${WARNINGS} -DDEADLINECODE_INDEPENDANT_EXE

# BEZIER
FILES =                              \
fitter_2d/bezier/bezier.o                      

# SHARE
FILES +=                            \
fitter_2d/share/line_intersection.o           \
fitter_2d/share/util.o                        \
fitter_2d/share/vtk_curve_writer.o            \
fitter_2d/share/projection_derivative.o            

# GLOBFIT
FILES +=                            \
fitter_2d/globfit/globfit_curve.o             \
fitter_2d/globfit/globfit_objective.o         \
fitter_2d/globfit/globfitter_marquart_levenberg.o  \
fitter_2d/globfit/globfitter.o                

# TESTS
FILES += fitter_2d/bezier/test/bezier_test_bezier.o
FILES += fitter_2d/bezier/test/bezier_test_vtk_writer.o
#
FILES += fitter_2d/globfit/test/globfit_test_objectives.o
FILES += fitter_2d/globfit/test/globfit_test_curves.o
FILES += fitter_2d/globfit/test/globfit_example.o

all: ${FILES}
	${CC} main_tests.cpp ${FILES} ${FLAGS}

%.o: %.cpp
	${CC} $< -c -o $@  ${FLAGS} 

.PHONY: clean depend

# thanks to Robert Bridson
depend:
	-rm -f .depend
	$(foreach objfile,${FILES},${DEPEND} -MM $(patsubst %.o,%.cpp, ${objfile})  ${FLAGS} -MT ${objfile} >> .depend;)


clean:
	rm -f ${FILES}



Makefile:;


-include .depend
